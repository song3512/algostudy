package baek;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_백준_1149_RGB거리_서울9반_송원석_1차원배열 {

	public static void main(String[] args) throws Exception {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		int N = Integer.parseInt(bf.readLine());
		
		int[] R = new int[N];
		int[] G = new int[N];
		int[] B = new int[N];
		for(int i=0;i<N;i++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			R[i] = Integer.parseInt(st.nextToken());
			G[i] = Integer.parseInt(st.nextToken());
			B[i] = Integer.parseInt(st.nextToken());
		}
		
		for(int i=1;i<N;i++) {
			R[i] = Math.min(R[i]+B[i-1], R[i]+G[i-1]);
			G[i] = Math.min(G[i]+B[i-1], G[i]+R[i-1]);
			B[i] = Math.min(B[i]+R[i-1], B[i]+G[i-1]);
		}
		
		int min = Math.min(R[N-1], G[N-1]);
		System.out.println(Math.min(min, B[N-1]));
	}

}
