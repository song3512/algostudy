package baek;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_백준_1149_RGB거리_서울9반_송원석2 {

	public static void main(String[] args) throws Exception {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		int N = Integer.parseInt(bf.readLine());
		
		int[][] value = new int[N][3];
		for(int i=0;i<N;i++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			value[i][0] = Integer.parseInt(st.nextToken());
			value[i][1] = Integer.parseInt(st.nextToken());
			value[i][2] = Integer.parseInt(st.nextToken());
			
		}
		
		for(int i=1;i<N;i++) {
			for(int j=0;j<3;j++) {
				if(j==0) {
					value[i][j] = Math.min(value[i-1][1]+ value[i][j],value[i-1][2]+ value[i][j] );
				}else if(j==1) {
					value[i][j] = Math.min(value[i-1][0]+ value[i][j],value[i-1][2]+ value[i][j] );
				}else {
					value[i][j] = Math.min(value[i-1][0]+ value[i][j],value[i-1][1]+ value[i][j] );
				}
			}
		}
		int min = Arrays.stream(value[N-1]).min().getAsInt(); //최소값 구하기
		
		System.out.println(min);
	}

}
