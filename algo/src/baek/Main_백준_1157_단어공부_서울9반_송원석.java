package a1011;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Main_백준_1157_단어공부_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		char[] word = bf.readLine().toUpperCase().toCharArray();
		HashMap<Character,Integer> hash = new HashMap<>();
		int max=1;
		char result=word[0];
		for(int i=0;i<word.length;i++) {
			if(hash.get(word[i]) == null)
				hash.put(word[i], 1);
			else {
				hash.put(word[i],hash.get(word[i])+1);
				if(hash.get(word[i]) > max) {
					max =hash.get(word[i]);
					result = word[i]; 
				}
			}
		}
		
		
		boolean check = true;
		for(int i=0;i<word.length;i++) {
			if(hash.get(word[i]) == max && word[i] != result) {
				check = false;
				break;
			}
		}
		
		if(check)
			System.out.println(result);
		else
			System.out.println("?");
		
		
	}

}
