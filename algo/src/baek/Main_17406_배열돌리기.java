package baek;
import java.io.*;
import java.util.*;

public class Main_17406_λ°°μ΄?λ¦¬κΈ°_??Έ9λ°?_?‘?? {
	static int[][] arr;
	static int[] di = { 0, 1, 0, -1 }; // ?°?μ’μ
	static int[] dj = { 1, 0, -1, 0 }; // ?°?μ’μ

	public static void rotation(int r, int c, int s) {
		if (s == 0) 
			return;
		int num = s * 2;
		Queue<Integer> que = new LinkedList<>();
		int nx = r - s;
		int ny = c - s;
		que.offer(arr[nx][ny]);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < num; j++) {
				nx = nx + di[i];
				ny = ny + dj[i];
				if(nx == r-s && ny == c-s)
					break;
				que.offer(arr[nx][ny]);
			}
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < num; j++) {
				nx = nx + di[i];
				ny = ny + dj[i];
				arr[nx][ny] = que.poll();
			}
		}

		rotation(r, c, s - 1);
	}

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_main17406.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine(), " ");
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int K = Integer.parseInt(st.nextToken());
		arr = new int[N][M];

		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(bf.readLine(), " ");
			for (int j = 0; j < M; j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		int r = 0;
		int c = 0;
		int s = 0;

		for (int k = 0; k < K; k++) {
			st = new StringTokenizer(bf.readLine(), " ");
			r = Integer.parseInt(st.nextToken()) - 1;
			c = Integer.parseInt(st.nextToken()) - 1;
			s = Integer.parseInt(st.nextToken());
			if (r - s < 0 || r - s >= N || c - s < 0 || c - s >= M)
				continue;
			rotation(r, c, s);
		}
		int sum = 0;
		int min = Integer.MAX_VALUE/2;
		for (int i = 0; i < N; i++) {
			sum = 0;
			for (int j = 0; j < M; j++) {
				sum += arr[i][j];
			}
			min = Math.min(sum, min);
		}
		System.out.println(min);
	}
}
