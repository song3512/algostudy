package baek;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

class Shark2 { // �Ʊ���� Ŭ���� ��ħ
	int x, y, size, dir, speed;
	public Shark2(int x, int y, int speed, int dir, int size) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.dir = dir;
		this.size = size;
	}
	public void reverseDirect() {
		if (this.dir == 0)
			this.dir = 1;
		else if (this.dir == 1)
			this.dir = 0;
		else if (this.dir == 2)
			this.dir = 3;
		else
			this.dir = 2;
	}
}

public class Main_����_17143_���ÿ� {
	static int R, C, M, mR, mC;
	static HashMap<Integer, Shark2> hash;
	static ArrayList<Shark2> list, die;

	public static void move(Shark2 s) {
		int[] dx = { -1, 1, 0, 0 }, dy = { 0, 0, 1, -1 }; // ���Ͽ���

		if (s.speed != 0) {
			int nx = s.x;
			int ny = s.y;
			for (int i = 0; i < s.speed; i++) {
				if (nx + dx[s.dir] < 0 || nx + dx[s.dir] >= R || ny + dy[s.dir] < 0 || ny + dy[s.dir] >= C) {
					s.reverseDirect();
				}
				if (nx + dx[s.dir] >= 0 && nx + dx[s.dir] < R && ny + dy[s.dir] >= 0 && ny + dy[s.dir] < C) {
					nx += dx[s.dir];
					ny += dy[s.dir];
				}
			}
			s.x = nx;
			s.y = ny;
		}
		
		if (!hash.containsKey(s.x * C + s.y))
			hash.put(s.x * C + s.y, s);
		else {
			if (hash.get(s.x * C + s.y).size > s.size)
				die.add(s);
			else {
				die.add(hash.get(s.x * C + s.y));
				hash.put(s.x * C + s.y, s);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine());
		R = Integer.parseInt(st.nextToken());
		C = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		mR = 2 * R - 2;
		mC = 2 * C - 2;
		hash = new HashMap<>();
		list = new ArrayList<>();
		die = new ArrayList<>();
		int total = 0;
		for (int i = 0; i < M; i++) {
			st = new StringTokenizer(bf.readLine());
			int r = Integer.parseInt(st.nextToken()) - 1;
			int c = Integer.parseInt(st.nextToken()) - 1;
			int s = Integer.parseInt(st.nextToken());
			int d = Integer.parseInt(st.nextToken()) - 1;
			int z = Integer.parseInt(st.nextToken());
			if (d == 0 || d == 1) {
				if(mR != 0)
					s = s % mR;
				else s=0;
			} else
				if(mC != 0)
					s = s % mC;
				else
					s=0;
			list.add(new Shark2(r, c, s, d, z));
			hash.put(r * C + c, list.get(i));
		}
		
		
		for (int c = 0; c < C; c++) {
			if(list.size()==0)
				break;
			for (int r = 0; r < R; r++) {
				if (hash.containsKey(r * C + c)) {
					total += hash.get(r * C + c).size;
					list.remove(hash.get(r * C + c));
					break;
				}
			}
			die.clear();
			hash.clear();
			for (int i = 0; i < list.size(); i++)
				move(list.get(i));
			for (int i = 0; i < die.size(); i++) {
				list.remove(die.get(i));

			}
		}
		System.out.println(total);
	}
}
