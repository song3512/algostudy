package baek;

import java.io.*;

public class Main_11726_2xnŸ�ϸ� {
	
	static int d[];
	
	public static int topDown(int n) {
		if(n <=1) {
			return 1;
		}
		if(d[n] >0) return d[n];
		
		d[n] = topDown(n-2)+ topDown(n-1);
		d[n] %= 10007;
		return d[n];
	}
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		d = new int[n+1];
		System.out.println(topDown(n));
	}

}
