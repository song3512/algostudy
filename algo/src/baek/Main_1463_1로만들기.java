package baek;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main_1463_1�θ���� {
	static int min;
	public static void solve(int num, int count) {
		if(count >= min)
			return;
		if(num == 1) {
			min = Math.min(min, count);
			return;
		}
		
		if(num %3==0) {
			solve(num/3,count+1);
			solve(num-1,count+1);
		}if(num%2==0) {
			solve(num/2,count+1);
			solve(num-1,count+1);
		}else {
			solve(num-1,count+1);
		}
		
	}
	

	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int num = Integer.parseInt(br.readLine());
		
		min = Integer.MAX_VALUE;
		
		solve(num,0);
		System.out.println(min);
		
	}

}
