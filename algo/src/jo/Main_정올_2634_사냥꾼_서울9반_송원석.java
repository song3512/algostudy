package jo;

import java.io.*;
import java.util.*;


class Point implements Comparable<Point>{
	int r;
	int c;
	public Point(int r, int c) {
		this.r = r;
		this.c = c;
	}
	public int compareTo(Point o) {
		return Integer.compare(this.r+this.c, o.r+o.c);
	}
}
public class Main_정올_2634_사냥꾼_서울9반_송원석 {
	public static void main(String[] args) throws Exception{
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine(), " ");
		
		int M = Integer.parseInt(st.nextToken());
		int N = Integer.parseInt(st.nextToken());
		int L = Integer.parseInt(st.nextToken());
		int[] hunter = new int[M];
		st = new StringTokenizer(bf.readLine(), " ");
		for(int i=0;i<M;i++)
			hunter[i] = Integer.parseInt(st.nextToken());
		Arrays.sort(hunter);
		
		PriorityQueue<Point> pq = new PriorityQueue<>();
		
		for(int i=0;i<N;i++) {
			st = new StringTokenizer(bf.readLine(), " ");
			int m=Integer.parseInt(st.nextToken());
			int n=Integer.parseInt(st.nextToken());
			pq.offer(new Point(m,n));
		}
		int ans =0;
		for(int i=0;i<hunter.length;i++) {
			int cnt=0;
			while(!pq.isEmpty()) {
				Point cur = pq.peek();
				if((Math.abs(hunter[i]-cur.r)+cur.c) <=L) {
					cnt++;
					pq.poll();
				}else {
					if(cur.r < hunter[i]) {
						pq.poll();
					}else {
						break;
					}
				}
			}
				ans += cnt;
		}
		System.out.println(ans);
	}

}
