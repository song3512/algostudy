package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.StringTokenizer;

public class Main_정올_2097_지하철_서울9반_송원석_dijkstra {
	static int N,M,min;
	static int[] d,p;
	static int[][] graph;
	static boolean[] v;
	static String way;
	
	public static void dijkstra(int start) {
		d[start]=0;
		for(int i=0;i<N-2;i++) {
			int curr=getSmallIndex();
			v[curr] = true;
			for(int j=0;j<N;j++) {
				if(!v[j]) {
					if(d[j]>d[curr]+graph[curr][j]) {
						d[j]=d[curr]+graph[curr][j];
						p[j]=curr;
					}
				}
			}
		}
	}
	
	private static int getSmallIndex() {
		int min =Integer.MAX_VALUE;
		int index=0;
		for(int i=0;i<N;i++) {
			if(d[i]<min &&!v[i]) {
				min = d[i];
				index=i;
			}
		}
		return index;
	}


	public static void main(String[] args) throws Exception{
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken())-1;
		d= new int[N];
		p= new int[N];
		graph = new int[N][N];
		v = new boolean[N];
		
		for(int i=0;i<N;i++) {
			st = new StringTokenizer(bf.readLine());
			for(int j=0;j<N;j++) {
				graph[i][j] = Integer.parseInt(st.nextToken());
			}
			d[i] = Integer.MAX_VALUE;
			p[i] = -1;
		}
		dijkstra(0);
		System.out.println(d[M]);
		Stack<Integer> path = new Stack<>();
		for(int k=M;k!=-1;k=p[k]) {
			path.push(k+1);
		}
		while(!path.isEmpty()) {
			System.out.print(path.pop()+" ");
		}
		System.out.println();
		
		
	}
}
