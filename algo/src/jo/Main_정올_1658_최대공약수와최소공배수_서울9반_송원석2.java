package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_정올_1658_최대공약수와최소공배수_서울9반_송원석2 {

	public static void main(String[] args) throws Exception{
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine());
		BigInteger[] num = new BigInteger[2];
		num[0] = new BigInteger(st.nextToken());
		num[1] = new BigInteger(st.nextToken());
		BigInteger GCD=num[0].gcd(num[1]);
		BigInteger LCM= num[0].multiply(num[1]).divide(GCD);
		System.out.println(GCD);
		System.out.println(LCM);
		
	}

}
