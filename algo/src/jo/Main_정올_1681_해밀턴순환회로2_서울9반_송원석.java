package jo;
import java.io.*;
import java.util.*;

public class Main_정올_1681_해밀턴순환회로2_서울9반_송원석 {
	static int[][] graph;
	static int N, INF = Integer.MAX_VALUE/2;
	static int[][] m;
	
	public static int tsp(int pos, int visited) {
		if(m[pos][visited]!=0) return m[pos][visited];
		
		if(visited == (1<<N)-1) return m[pos][visited]=graph[pos][0];
		int ret=INF;
		int tmp=0;
		for(int next=1;next<N;next++) {
			if((visited&(1<<next)) ==0 && graph[pos][next] !=0) {
				tmp = graph[pos][next] + tsp(next,visited|(1<<next));
				ret = Math.min(ret, tmp);
			}
		}
		m[pos][visited]=ret;
		return ret;
	}
	
	public static void main(String[] args) throws Exception {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(bf.readLine());
		graph = new int[N][N];
		m = new int[N][1<<N];
		StringTokenizer st = null;
		for(int i=0;i<N;i++) {
			st = new StringTokenizer(bf.readLine());
			for(int j=0;j<N;j++) {
				graph[i][j] = Integer.parseInt(st.nextToken());
				if(i!=j && graph[i][j] == 0)
					graph[i][j] = INF;
			}
		}
		int result = tsp(0,1<<0);
		System.out.println(result);
		
	}

}
