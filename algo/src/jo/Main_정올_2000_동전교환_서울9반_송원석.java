package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_정올_2000_동전교환_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(bf.readLine());
		int coin[] = new int[N];
		StringTokenizer st = new StringTokenizer(bf.readLine(), " ");
		for(int i=0;i<N;i++) {
			coin[i] = Integer.parseInt(st.nextToken());
		}
		int W= Integer.parseInt(bf.readLine());
		int[] m = new int[W + 1];
		for (int i = 1; i <= W; i++) {
			int min = Integer.MAX_VALUE/2;
			for (int j = 0; j < N; j++) {
				if (i-coin[j] >= 0) {
					if (min > m[i - coin[j]])
						min = m[i - coin[j]];
				}
			}
				m[i] = min + 1;
		}
		if(m[W] == Integer.MAX_VALUE/2+1)
			System.out.println("impossible");
		else
			System.out.println(m[W]);
	}
	

}
