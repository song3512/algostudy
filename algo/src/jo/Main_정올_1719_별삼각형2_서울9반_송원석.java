package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_정올_1719_별삼각형2_서울9반_송원석 {

	public static void starPrint(int size,int num) {
		
		StringBuilder sb = new StringBuilder();
		int mid = size/2;
		int length=0;
		switch(num) {
		case 1:
			for(int i=0;i<size;i++) {
				for(int j=0;j<=length;j++) {
					sb.append("*");
				}
				sb.append("\n");
				if(i>=mid)
					length--;
				else
					length++;
			}
			break;
		case 2:
			for(int i=0;i<size;i++) {
				for(int j=0;j<=mid;j++) {
					if(j>=mid-length)
						sb.append("*");
					else
						sb.append(" ");
				}
				sb.append("\n");
				if(i>=mid)
					length--;
				else
					length++;
			}
			break;
		case 3:
			for(int i=0;i<size;i++) {
				for(int j=0;j<size;j++) {
					if(j<length)
						sb.append(" ");
					else if(j >size-length-1)
						sb.append(" ");
					else
						sb.append("*");
				}
				sb.append("\n");
				if(i>=mid)
					length--;
				else
					length++;
			}
			break;
		default:
			int length1=0;
			int length2=mid;
			for(int i=0;i<size;i++) {
				for(int j=0;j<size;j++) {
					if(j<length1)
						sb.append(" ");
					else if(j >length2)
						sb.append(" ");
					else
						sb.append("*");
				}
				sb.append("\n");
				if(i>=mid)
					length2++;
				else
					length1++;
			}
			
		}
		System.out.print(sb);
		
	}
	
	public static void main(String[] args) throws Exception{
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine());
		
		int n = Integer.parseInt(st.nextToken());
		int m = Integer.parseInt(st.nextToken());
		if( n%2 == 0 || n>100 || m <1 || m >4 )
			System.out.println("INPUT ERROR!");
		else
			starPrint(n,m);
	}

}
