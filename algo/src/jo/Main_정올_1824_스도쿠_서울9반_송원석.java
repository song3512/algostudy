package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.StringTokenizer;

public class Main_정올_1824_스도쿠_서울9반_송원석 {
	static int N;
	static int[][] board;
	static ArrayList<Integer> list;

	public static void sdoku(int r) {
		if(r==N) {
			for(int[] a: board) {
				for(int b : a) {
					System.out.print(b+" ");
				}
				System.out.println();
			}
			System.exit(0);
		}
		int x = list.get(r)/9;
		int y = list.get(r)%9;
		ArrayList<Integer> list2 = possibleNum(x,y);
		for(int i=0;i<list2.size();i++) {
			board[x][y] = list2.get(i);
			sdoku(r+1);
			board[x][y] =0;
		}
		
	}
	//범위나누기
	public static ArrayList<Integer> possibleNum(int x,int y){
		HashSet<Integer> set = new HashSet<>();
		int sx = x/3;
		int sy = y/3;
		boolean[] v= new boolean[10];
		for(int i=sx*3;i<sx*3+3;i++) {
			for(int j=sy*3;j<sy*3+3;j++) {
				if(board[i][j] !=0) {
					v[board[i][j]] = true;
				}
			}
		}
		for(int i=1;i<=9;i++) {
			if(!v[i])
				set.add(i);
		}
		
		//가로 세로 비교
		for(int i=0;i<9;i++) {
			if(set.contains(board[x][i]))
				set.remove(board[x][i]);
			if(set.contains(board[i][y]))
				set.remove(board[i][y]);
		}
		ArrayList<Integer> list = new ArrayList<>();
		list.addAll(set);
		return list;
	}
	public static void main(String[] args) throws Exception {
		board = new int[9][9];
		list = new ArrayList<>();
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st =null;
		
		for(int i=0;i<9;i++) {
			st = new StringTokenizer(bf.readLine());
			for(int j=0;j<9;j++) {
				board[i][j] = Integer.parseInt(st.nextToken());
				if(board[i][j] ==0) {
					list.add(i*9+j);
				}
			}
		}
		N = list.size();
		sdoku(0);
	}

}
