package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

public class Main_정올_1733_오목_서울9반_송원석 {
	static int[][] map;
	static int N = 19;
	static int[][] spot;
	public static void main(String[] args) throws Exception {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		map = new int[N][N];
		spot = new int[5][2];
		boolean flag = false;
		for(int i=0;i<N;i++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			for (int j = 0; j < N; j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		int result=0;
label:	for(int i=0;i<N;i++) {
			for(int j=0;j<N;j++) {
				if(map[i][j] == 1 || map[i][j] == 2) {
					if(check(i,j, map[i][j])) {
						result=map[i][j];
						Arrays.sort(spot,new Comparator<int[]>() {
							@Override
							public int compare(int[] o1, int[] o2) {
								if(o1[1]==o2[1]) {
									return Integer.compare(o1[0], o2[0]);
								}
								return Integer.compare(o1[1], o2[1]);
							}
						});
						flag = true;
						break label;
					}
				}
			}
		}
		
		
		if(flag) {
			System.out.println(result);
			System.out.println(spot[0][0]+" "+spot[0][1]);
		}else {
			System.out.println(result);
		}
	}
	
	
	public static boolean check(int x,int y, int color) {
		//12시방향부터 시계방향으로 이동
		int[] dx= {-1,-1,0,1,1,1,0,-1}, dy= {0,1,1,1,0,-1,-1,-1};
		for(int dir=0;dir<dx.length;dir++) {
			int nx=x;
			int ny=y;
			int cnt=0;
			spot[cnt][0]= nx+1;
			spot[cnt][1]= ny+1;
			while(true) {
				nx +=dx[dir];
				ny +=dy[dir];
				if(nx <0 || nx >=N || ny<0 || ny>=N || map[nx][ny] != color)
					break;
				cnt++;
				if(cnt >=5)
					return false;
				spot[cnt][0]=nx+1;
				spot[cnt][1]=ny+1;
			}
			if(cnt==4) {
				nx = x+dx[(dir+4)%8];
				ny = y+dy[(dir+4)%8];
				if(nx >=0 && nx <N && ny >=0 && ny<N && map[nx][ny] == color)
					return false;
				return true;
			}
				
		}
		
		return false;
	}
	

}
