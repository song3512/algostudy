package jo;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main_����� {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		
		int[][] matter = new int[N][2];
		for(int i=0;i<N;i++) {
			matter[i][0] = sc.nextInt();
			matter[i][1] = sc.nextInt();
		}
		
		Arrays.sort(matter,new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return Integer.compare(o1[1],o2[1]);
			}
		});
		int cnt=1;
		int max=matter[0][1];
		for(int i=1;i<N;i++) {
			if(max <matter[i][0]) {
				cnt++;
				max=matter[i][1];
			}
		}
		System.out.println(cnt);
		sc.close();
	}
}


