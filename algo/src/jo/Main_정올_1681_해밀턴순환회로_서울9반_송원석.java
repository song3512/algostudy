package jo;

import java.io.*;
import java.util.*;

public class Main_정올_1681_해밀턴순환회로_서울9반_송원석 {
	static int[] p;
	static int[][] graph;
	static int N, min;
	static boolean[] visit;
	public static void dfs(int curr,int sum, int cnt) {
		if(cnt==N) {
			if(graph[curr][0] == 0)
				return;
			min = Math.min(min, sum+graph[curr][0]);
			return;
		}
		for(int i=1;i<N;i++) {
			if(!visit[i] && graph[curr][i] != 0 && sum+graph[curr][i]<min) {
				visit[i] = true;
				dfs(i,sum+graph[curr][i],cnt+1);
				visit[i] = false;
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(bf.readLine());
		p = new int[N];
		graph = new int[N][N];
		min = Integer.MAX_VALUE;
		for(int i=0;i<N;i++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			for(int j=0;j<N;j++) {
				graph[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		visit = new boolean[N];
		
		visit[0] = true;
		dfs(0,0,1);
		
		System.out.println(min);
		
	}

}
