package jo;

import java.io.*;
import java.util.*;

public class Main_정올_1840_치즈_서울9반_송원석_bfs {
	public static int N, M;
	public static int[] di = { -1, 1, 0, 0 };
	public static int[] dj = { 0, 0, -1, 1 };

	public static int[][] cheese;

	public static int countCheese() {
		int cnt = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				cnt += cheese[i][j];
			}
		}

		return cnt;
	}

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		cheese = new int[N][M];
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < M; j++) {
				cheese[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		int day = 0;
		Queue<int[]> que = new LinkedList<>();
		
		int result=0;
		while (true) {
			que.offer(new int[] { 0, 0 });
			boolean[][] v = new boolean[N][M];
			v[0][0] = true;
			while (!que.isEmpty()) {
				int[] xy = que.poll();
				

				if (cheese[xy[0]][xy[1]] == 1) {
					cheese[xy[0]][xy[1]] = 0;
					continue;
				}

				for (int i = 0; i < 4; i++) {
					int nx = xy[0] + di[i];
					int ny = xy[1] + dj[i];
					if (nx >= 0 && nx < N && ny >= 0 && ny < M && !v[nx][ny]) {
						v[nx][ny] = true;
						que.offer(new int[] { nx, ny });
					}
				}
			}
			day++;
			if(countCheese() == 0)
				break;
			result = countCheese();
		}

		System.out.println(day);
		System.out.println(result);

	}

}
