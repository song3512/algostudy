package jo;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.Stack;

public class Main_정올_1695_단지번호붙이기_Dfs_서울9반_송원석 {
	static int[] di = {-1,1,0,0};
	static int[] dj = {0,0,-1,1};
	static int[][] ar;
	static int cnt;
	static int N;
	
	public static void dfs(int x,int y) {
		ar[x][y]=0;
		cnt++;
		for(int i=0;i<4;i++) {
			if(x+di[i] >=N || x+di[i]<0 || y+dj[i] >=N || y+dj[i]<0)
				continue;
			int nx= x+di[i];
			int ny= y+dj[i];
			if(ar[nx][ny] !=0)	
				dfs(nx,ny);
		}
	}

	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_jo_2667.txt"));
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		ar= new int[N][N];
		ArrayList<Integer> result = new ArrayList<>();
		for(int i=0;i<N;i++) {
			String line = sc.next();
			for(int j=0;j<N;j++) {
				ar[i][j] = line.charAt(j)-'0';
			}
		}
		for(int i=0;i<N;i++) {
			for(int j=0;j<N;j++) {
				if(ar[i][j] != 0) {
					cnt=0;
					dfs(i,j);
					result.add(cnt);
				}
			}
		}
		Collections.sort(result);
		int size = result.size();
		System.out.println(size);
		for(int i=0;i<size;i++)
			System.out.println(result.get(i));
	}
}
