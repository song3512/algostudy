package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_정올_2097_지하철_서울9반_송원석_dfs {
	static int N,M,min;
	static int[][] graph;
	static boolean[] v;
	static String way;
	public static void dfs(int start,int sum,String path) {
		if(sum >= min) {
			return;
		}
		if(start == M-1) {
			if(min > sum) {
				min= Math.min(min, sum);
				way = path;
			}
			return;
		}
		
		for(int i=0;i<N;i++) {
			if(start != i && !v[i]) {
				v[i] = true;
				dfs(i,sum+graph[start][i],path+" "+(i+1));
				v[i] = false;
			}
		}
		
		
	}
	
	
	public static void main(String[] args) throws Exception{
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		min = Integer.MAX_VALUE;
		graph = new int[N][N];
		v = new boolean[N];
		for(int i=0;i<N;i++) {
			st = new StringTokenizer(bf.readLine());
			for(int j=0;j<N;j++) {
				graph[i][j] = Integer.parseInt(st.nextToken());
				if(graph[i][j] == 0)
					graph[i][j] = Integer.MAX_VALUE;
			}
		}
		v[0] = true;
		dfs(0,0,Integer.toString(1));
		System.out.println(min);
		System.out.println(way);
		
	}
}
