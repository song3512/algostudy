package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_정올_1658_최대공약수와최소공배수_서울9반_송원석 {

	public static void main(String[] args) throws Exception{
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine());
		int num[] = new int[2];
		num[0] = Integer.parseInt(st.nextToken());
		num[1] = Integer.parseInt(st.nextToken());
		Arrays.sort(num);
		int GCD=0;
		int LCM=0;
		for(int i=1;i<=num[1];i++) {
			if(num[0]%i == 0 && num[1]%i==0)
				GCD=i;
		}
		
		LCM = num[0]*num[1]/GCD;
		System.out.println(GCD);
		System.out.println(LCM);
		
	}

}
