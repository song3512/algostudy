package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.StringTokenizer;

public class Main_정올_2577_회전초밥고_서울9반_송원석 {

	public static void main(String[] args) throws Exception{
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(bf.readLine().trim());
		int N = Integer.parseInt(st.nextToken());
		int d = Integer.parseInt(st.nextToken());
		int k = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		
		int[] sushi = new int[N];
		
		for(int i=0;i<N;i++)
			sushi[i] = Integer.parseInt(bf.readLine());
		
		int[] nums = new int[d+1];
		int result=1;
		nums[C]++;
		int max=0;
		int start=0;
		
		for(int i=0;i<N+k;i++) {
			if(nums[sushi[i%N]] ==0) result++;
			nums[sushi[i%N]]++;
			if(i >=k) {
				nums[sushi[start]]--;
				if(nums[sushi[start]] ==0) result--;
				start++;
			}
			max=Math.max(max, result);
			if(max==k+1)
				break;
		}
		System.out.println(max);
		
	}

}
