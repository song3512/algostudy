package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_정올_1840_치즈_서울9반_송원석_dfs {
	public static int N, M;
	public static int[] di = { -1, 1, 0, 0 };
	public static int[] dj = { 0, 0, -1, 1 };

	public static int[][] cheese;
	public static boolean[][] v;


	public static void dfs(int x, int y) {
		v[x][y] = true;
		if(cheese[x][y] ==1) {
			cheese[x][y] =0;
			return;
		}
		
		for (int i = 0; i < 4; i++) {
			int nx = x + di[i];
			int ny = y + dj[i];
			if (nx >= 0 && nx < N && ny >= 0 && ny < M && !v[nx][ny]) {
				dfs(nx, ny);
			}
		}
	}

	public static int countCheese() {
		int cnt = 0;
		for(int i=0;i<N;i++) {
			for(int j=0;j<M;j++) {
				cnt+=cheese[i][j];
			}
		}
		
		return cnt;
	}

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		cheese = new int[N][M];
		for (int i = 0; i < N; i++) {
			st = new StringTokenizer(br.readLine());
			for (int j = 0; j < M; j++) {
				cheese[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		int result=0;
		int time=0;
		while(true) {
			if(countCheese() ==0) {
				break;
			}
			result = countCheese();
			v= new boolean[N][M];
			dfs(0,0);
			time++;
		}
		
		System.out.println(time);
		System.out.println(result);

	}

}
