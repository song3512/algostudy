package jo;

import java.io.*;
import java.util.*;


public class Main_정올_1863_종교_서울9반_송원석 {
	
	public static void unionSet(int[] p,int x,int y) {
		x = findSet(p,x);
		y = findSet(p,y);
		if(x<y)
			p[y] = x;
		else
			p[x] = y;
	}
//	public static int updateParents(int[] p,int x) {
//		if(findSet(p,x) == findSet(p,findSet(p,x)))
//			return findSet(p,x);
//		return findSet(p,findSet(p,x));
//	}
	
	public static int findSet(int[] p,int x) {
		if(p[x] ==x)
			return x;
		return findSet(p,p[x]);
		
	}
	
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_jo_1863.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st = new StringTokenizer(bf.readLine()," ");
		
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		int[] p = new int[N+1];
		
		for(int i=1;i<=N;i++){
			p[i] = i;
		}
		int n1=0;
		int n2=0;
		for(int i=0;i<M;i++) {
			st = new StringTokenizer(bf.readLine()," ");
			n1 = Integer.parseInt(st.nextToken());
			n2 = Integer.parseInt(st.nextToken());
			unionSet(p,n1,n2);
		}
		
//		for(int i=1;i<=N;i++)
//			p[i]=updateParents(p,i);
		
		for(int i=1;i<=N;i++) {
			if(p[i] != findSet(p,p[i]))
				p[i] = findSet(p,p[i]);
		}
		
		int[] num = new int[N+1];
		
		for(int i=1;i<=N;i++) {
			num[p[i]]++;
		}
		int cnt=0;
		for(int i=1;i<=N;i++) {
			if(num[i] >=1)
				cnt++;
		}
		
		System.out.println(cnt);
	
	}

}
