package jo;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main_정올_2074_홀수마방진_서울9반_송원석 {

	public static void main(String[] args) throws Exception{
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(bf.readLine());
		int[][] map = new int[N][N];
		int x = 0;
		int y = N/2;
		for(int num=1;num<=N*N;num++) {
			map[x][y]=num;
			if((num)%N == 0) {
				x++;
			}else {
				x--;
				y--;
				if(x <0)
					x=N-1;
				if(y<0)
					y=N-1;
			}
			
		}
		
		for(int i=0;i<N;i++) {
			for(int j=0;j<N;j++) {
				System.out.print(map[i][j]+" ");
			}
			System.out.println();
		}
		
		
	}

}
