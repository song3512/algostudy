package jo;

import java.util.Scanner;

class Cn {
	int low;
	int high;
	boolean check = false;

	public Cn(int low, int high) {
		this.low = low;
		this.high = high;
	}
}

public class Main_1828_냉장고_서울9반_송원석 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		int result = 0;
		Cn[] cns = new Cn[N];
		for (int i = 0; i < N; i++) {
			cns[i]=new Cn(sc.nextInt(), sc.nextInt());
		}
		Cn temp = null;
		for (int i = 0; i < N-1; i++) {
			for(int j=i+1; j<N;j++) {
				if(cns[i].high > cns[j].high) {
					temp = cns[i];
					cns[i] = cns[j];
					cns[j] = temp;
				}
			}
		}
		int cnt = 0;
		while (cnt != N) {
			cnt = 0;
			int max = 0;
			int[] index = new int[N];
			int check = 0;
			result++;
			for (int i = 0; i < N; i++) {
				if (cns[i].check) {
					continue;
				}
				for (int j = 0; j < N; j++) {
					if (((cns[i].high <= cns[j].low && cns[i].low <= cns[j].low) || 
						(cns[i].low <= cns[j].high && cns[i].high >= cns[j].high)) &&!cns[j].check) {
						index[i]++;
					}
				}
			}
			for (int i = 0; i < N; i++) {
				if (index[i] > max) {
					max = index[i];
					check = i;
				}
			}
			for (int j = 0; j < N; j++) {
				if (((cns[check].low <= cns[j].low && cns[check].high >= cns[j].low) || 
						(cns[check].low <= cns[j].high && cns[check].high >= cns[j].high)) &&!cns[j].check) {
					cns[j].check = true;
				}
			}
			for (int i = 0; i < N; i++) {
			if (cns[i].check) {
				cnt++;
			}
			}
		}
		System.out.println(result);
	}
}
