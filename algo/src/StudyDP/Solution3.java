package StudyDP;

import java.util.*;

public class Solution3 {
	 public static int[] solution(int[] progresses, int[] speeds) {
	        int[] answer;
	        Queue<Integer> que = new LinkedList<>();
	        ArrayList<Integer> list = new ArrayList<>();
	        for(int i=0;i<speeds.length;i++) {
	        	int rest = 100-progresses[i];
	        	if(rest%speeds[i] == 0) {
	        		que.add(rest/speeds[i]);
	        	}else {
	        		que.add(rest/speeds[i] + 1);
	        	}
	        }
	        while(!que.isEmpty()) {
	        	int num = que.poll();
	        	int cnt = 1;
	        	while(!que.isEmpty()) {
	        		if(num >= que.peek()) {
	        			que.poll();
	        			cnt++;
	        		}else {
	        			break;
	        		}
	        	}
	        	list.add(cnt);
	        }
	        answer = new int[list.size()];
	        for(int i=0;i<list.size();i++)
	        	answer[i] = list.get(i);
	        return answer;
	    }
	public static void main(String[] args) {
		int[] p = {93,30,55};
		int[] s = {1,30,5};
		int[] a = solution(p,s);
		System.out.println(Arrays.toString(a));
	}

}
