package StudyDP;


public class Solution1 {
	public static String solution(String s) {
	      String answer = "";
	      String[] words = s.split(" ");
	      for(int i=0;i<words.length;i++) {
	    	  if(i!=0)
	    		  answer+=" ";
	    	  words[i] = words[i].toLowerCase();
	    	  char[] word = words[i].toCharArray();
	    	  word[0] = Character.toUpperCase(word[0]);
	    	  words[i] = String.valueOf(word);
	    	  answer += words[i];
	      }
	      return answer;
	  }
	public static void main(String[] args) {
		String s = "bnm a3a";
		System.out.println(solution(s));
		
	}

}
