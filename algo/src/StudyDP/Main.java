package StudyDP;

import java.io.*;
import java.util.*;

public class Main {
	
    public static void main(String[] args) throws Exception {
    	BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
    	int N = Integer.parseInt(bf.readLine());
    	int[] T = new int[N+1];
    	int[] P = new int[N+1];
    	int[] DP = new int[N+1];
    	int result = 0;
    	for(int i=1;i<=N;i++) {
    		StringTokenizer st = new StringTokenizer(bf.readLine());
    		T[i] = Integer.parseInt(st.nextToken());
    		P[i] = Integer.parseInt(st.nextToken());
    	}
    	
    	for(int i=1;i<=N;i++) {
    		if(i+T[i] <= N+1) {
    			DP[i] = P[i];
    			int max =0;
    			for(int j=1;j<i;j++) {
    				if(j+T[j] <= i && DP[j] > max) {
    					max = DP[j];
    				}
    			}
    			DP[i] += max;
    			if(DP[i] > result) {
    				result = DP[i];
    			}
    		}
    	}
    	System.out.println(result);
    	

    }

}
