package StudyDP;

import java.util.Arrays;

public class Solution2 {
	 public int solution(int[] arr) {
	      int answer = 0;
	      Arrays.sort(arr);
	      int max = arr[arr.length-1];
	      int num=1;
	      while(true) {
	    	  boolean check = true;
	    	  answer = max*num;
	    	  for(int i=0;i<arr.length-1;i++) {
	    		  if((answer)%arr[i] !=0) {
	    			  check = false;
	    			  break;
	    		  }
	    	  }
	    	  if(check)
	    		  break;
	    	  num++;
	      }
	      return answer;
	  }
	public static void main(String[] args) {
		
	}

}
