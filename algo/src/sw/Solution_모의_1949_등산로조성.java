package sw;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Solution_모의_1949_등산로조성 {
	static int N,K,high,result;
	static int[][] map;
	static int[] di= {-1,1,0,0}, dj= {0,0,-1,1};
	static boolean[][] v;
	static ArrayList<int[]> list;
	
	public static void main(String[] args) throws Exception{
		//System.setIn(new FileInputStream("res/input_1949.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(bf.readLine());
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			N = Integer.parseInt(st.nextToken());
			K = Integer.parseInt(st.nextToken());
			map = new int[N][N];
			v = new boolean[N][N];
			list = new ArrayList<>();
			result=0;
			int max=0;
			for(int i=0;i<N;i++) {
				st = new StringTokenizer(bf.readLine());
				for(int j=0;j<N;j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
				max = Math.max(max, map[i][j]);
				}
			}
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					if(max == map[i][j]) {
						list.add(new int[] {i,j});
					}
				}
			}
			for(int i=0;i<list.size();i++) {
				v[list.get(i)[0]][list.get(i)[1]] = true;
				findRoad(list.get(i)[0],list.get(i)[1],1,false);
				v[list.get(i)[0]][list.get(i)[1]] = false;
			}
			System.out.println("#"+tc+" "+result);
		}//tc
	}

	public static void findRoad(int x, int y, int cnt,boolean check) {
		int nx = x;
		int ny = x;
		int c =0;
		if(map[x][y] <= 0) {
			result=Math.max(result, cnt);
			return;
		}
		for(int dir=0;dir<4;dir++) {
			nx = x+di[dir];
			ny = y+dj[dir];
			if(nx>=0 && nx <N && ny >=0 && ny <N && !v[nx][ny]) {
				if(map[x][y] > map[nx][ny]) {
					v[nx][ny] = true;
					findRoad(nx,ny,cnt+1,check);
					c++;
					v[nx][ny] = false;
				}else if(!check) {
					if(map[nx][ny]-K < map[x][y]) {
						v[nx][ny] = true;
						int temp =map[nx][ny];
						map[nx][ny] = map[x][y]-1;
						findRoad(nx,ny,cnt+1,!check);
						c++;
						v[nx][ny] = false;
						map[nx][ny] = temp;
					}
				}
			}
		}
		if(c==0) {
			result=Math.max(result, cnt);
			return;
		}
	}

}
