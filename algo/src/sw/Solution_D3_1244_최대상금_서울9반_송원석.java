package D3;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D3_1244_최대상금_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
//		System.setIn(new FileInputStream("res/input_1244.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();

		for (int tc = 1; tc <= T; tc++) {
			String line = sc.next();
			int num = sc.nextInt();
			int cnt = 0;
			int ar[] = new int[line.length()];
			
			
			for (int i = 0; i < line.length(); i++) {
				ar[i] = Character.getNumericValue(line.charAt(i));
			}

			label: for (int i = 0; i < ar.length - 1; i++) {
				int temp = ar[i];
				int max = ar[i];
				int index = i;
				for (int j = i + 1; j < ar.length; j++) {
					if (ar[j] >= max) {
						max = ar[j];
						index = j;
					}
				}
				if (temp > ar[i]) {
					ar[index] = ar[i];
					ar[i] = temp;
					cnt++;
					if(temp>max)
						max=temp;
				}
				
				if (cnt == num)
					break label;
			}

			if (cnt < num) {
				int n = ar.length - 1;
				int temp = 0;
				boolean flag = false;
				for (int i = 0; i < n; i++) {
					if (ar[i] == ar[i + 1]) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					if ((num - cnt) % 2 == 1) {
						temp = ar[n];
						ar[n] = ar[n - 1];
						ar[n - 1] = temp;
					}
				}
			}
			System.out.print("#" + tc + " ");
			for (int s : ar)
				System.out.print(s);
			System.out.println();

		} // tc

	}

}
