package sw;

public class Solution {
	static int max;
	
	static void dfs(int r,int maxindex,int preNum, int sum, int[][] land) {
		
		if(r== maxindex) {
			max = Math.max(max, sum);
			return;
		}
		for(int i=0;i<4;i++) {
			if(i != preNum)
				dfs(r+1,maxindex,i,sum+land[r][i],land);
		}
		
	}
	
	
    static int solution(int[][] land) {
    	
    	max=0;
    	int N = land.length;
    	
    	for(int i=0;i<4;i++) {
    		dfs(1,N,i,land[0][i],land);
    	}
    	
        return max;
    }


	public static void main(String[] args) {
		int[][] land = {{1,2,3,5}, {5,6,7,8}, {4,3,2,1}};
		
		System.out.println(solution(land));
		
	}

}
