package algoWS;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D4_1226_미로1_Dfs_서울9반_송원석 {

	static boolean[][] visit;
	static int[][] ar;
	static int[] di = { -1, 1, 0, 0 };
	static int[] dj = { 0, 0, -1, 1 };
	static boolean flag;

	public static void dfs(int x, int y) {
		if (ar[x][y] == 3) {
			flag = true;
			return;
		}

		for (int i = 0; i < 4; i++) {
			int nx = x + di[i];
			int ny = y + dj[i];
			if (nx >= 16 || nx < 0 || ny >= 16 || ny < 0)
				continue;

			if (ar[nx][ny] != 1 && visit[nx][ny] == false) {
				visit[nx][ny] = true;
				dfs(nx, ny);
			}

		}
	}

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1226.txt"));
		Scanner sc = new Scanner(System.in);
		for (int tc = 1; tc <= 10; tc++) {
			int T = sc.nextInt();
			ar = new int[16][16];
			int[] start = new int[2];
			visit = new boolean[16][16];
			flag = false;
			for (int i = 0; i < 16; i++) {
				String line = sc.next();
				for (int j = 0; j < 16; j++) {
					ar[i][j] = line.charAt(j) - '0';
					if (ar[i][j] == 2) {
						start[0] = i;
						start[1] = j;
					}
				}
			}

			visit[start[0]][start[1]] = true;
			dfs(start[0], start[1]);

			System.out.println("#" + T + " " + ((flag) ? 1 : 0));

		} // tc

	}

}
