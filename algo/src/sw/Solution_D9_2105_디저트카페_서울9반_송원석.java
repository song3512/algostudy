package sw;

import java.io.*;
import java.util.*;

public class Solution_D9_2105_디저트카페_서울9반_송원석 {
	static int N,left,right,max;
	static int[][] cafe;
	static int[] di = {1,1,-1,-1}, dj= {-1,1,1,-1}; // 대각선 좌측아래부터 이동
	static HashSet<Integer> set;
	
	public static void move(int dir, int x,int y, int sx, int sy, int left,int right) {
		if(x == sx && y == sy && set.size() != 1) {
			max = Math.max(max, set.size());
			return;
		}
		int nx,ny;
		if(dir == 0) {
			nx =x+di[0];
			ny =y+dj[0];
			if(nx>=0&& nx <N && ny >=0 && ny <N && !set.contains(cafe[nx][ny])) {
				set.add(cafe[nx][ny]);
				move(0,nx,ny,sx,sy,left+1,right);
				set.remove(cafe[nx][ny]);
			}
			nx = x+di[1];
			ny = y+dj[1];
			if(nx>=0&& nx <N && ny >=0 && ny <N && !set.contains(cafe[nx][ny]) && set.size()>1) {
				set.add(cafe[nx][ny]);
				move(1,nx,ny,sx,sy,left,right+1);
				set.remove(cafe[nx][ny]);
			}
		}else if(dir ==1) {
			nx =x+di[1];
			ny =y+dj[1];
			if(nx>=0&& nx <N && ny >=0 && ny <N && !set.contains(cafe[nx][ny])) {
				set.add(cafe[nx][ny]);
				move(1,nx,ny,sx,sy,left,right+1);
				set.remove(cafe[nx][ny]);
			}
			nx =x+di[2];
			ny =y+dj[2];
		if(nx>=0&& nx <N && ny >=0 && ny <N && !set.contains(cafe[nx][ny]) && left > 0) {
				set.add(cafe[nx][ny]);
				move(2,nx,ny,sx,sy,left-1,right);
				set.remove(cafe[nx][ny]);
			}
		}else if(dir ==2) {
			nx =x+di[2];
			ny =y+dj[2];
			if(nx>=0&& nx <N && ny >=0 && ny <N && !set.contains(cafe[nx][ny]) && left >0) {
				set.add(cafe[nx][ny]);
				move(2,nx,ny,sx,sy,left-1,right);
				set.remove(cafe[nx][ny]);
			}
			nx =x+di[3];
			ny =y+dj[3];
			if(nx>=0&& nx <N && ny >=0 && ny <N && (!set.contains(cafe[nx][ny]) || (nx == sx&& ny == sy)) && right>0) {
				set.add(cafe[nx][ny]);
				move(3,nx,ny,sx,sy,left,right-1);
				if(nx != sx || ny != sy)
					set.remove(cafe[nx][ny]);
			}
		}else{
			nx =x+di[3];
			ny =y+dj[3];
			if(nx>=0&& nx <N && ny >=0 && ny <N && (!set.contains(cafe[nx][ny]) || (nx == sx&& ny == sy)) && right >0) {
				set.add(cafe[nx][ny]);
				move(3,nx,ny,sx,sy,left,right-1);
				if(nx != sx || ny != sy)
					set.remove(cafe[nx][ny]);
			}
		}
	}
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_2105.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		
		for(int tc=1;tc<=T;tc++) {
			N = Integer.parseInt(bf.readLine()); 
			cafe = new int[N][N];
			max=0;
			set = new HashSet<>();
			StringTokenizer st = null;
			
			for(int i=0;i<N;i++) {
				st = new StringTokenizer(bf.readLine());
				for(int j=0;j<N;j++) {
					cafe[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			
			for(int i=0;i<N-1;i++) {
				for(int j=1;j<N-1;j++) {
					set.add(cafe[i][j]);
					move(0,i,j,i,j,0,0);
					set.clear();
				}
			}
			
			
			if(max == 0) {
				System.out.println("#"+tc+" "+-1);
			}else
				System.out.println("#"+tc+" "+max);
		}
		
	}

}
