package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_2068 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2068.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int max =0;
			
			for(int i=0;i<10;i++) {
				int num = Integer.parseInt(st.nextToken());
				if(num > max)
					max = num;
			}
			
			System.out.println("#"+tc+" "+max);
			
		}
		
		
	}

}
