package D4;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class D4_7854_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_7854.txt"));
		Scanner sc = new Scanner(System.in);

		int T = sc.nextInt();

		for (int tc = 1; tc <= T; tc++) {
			int count = 0;
			int N = sc.nextInt();
			long K = 10;
			while(true) {
				
				if(K/10 <=N)
					count++;
				else if(K/10 >N)
					break;
				if(K/8 <= N && K>=1000)
					count++;
				else if(K/8 >N && K>=1000)
					break;
				if(K/5 <= N)
					count++;
				else if(K/5 >N)
					break;
				if(K/4 <= N && K>=100)
					count++;
				else if(K/4 >N && K>=100)
					break;
				if(K/2 <= N)
					count++;
				else if(K/2 >N)
					break;
				
				K*=10;
			}
			
			
			System.out.println("#" + tc + " " + count);
		}

	}

}
