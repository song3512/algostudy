package D4;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Scanner;

class Tree {
	int left;
	int right;
	String data;

	public Tree() {
	}

	public Tree(String data) {
		this.data = data;
		this.left = 0;
		this.right = 0;
	}

	public Tree(int left, int right, String data) {
		this.left = left;
		this.right = right;
		this.data = data;
	}
}

public class Solution_D4_1232_사칙연산_서울9반_송원석 {
	public static ArrayList<Tree> ar;

	public static int order(int n) {
		int L = 0;
		int R = 0;
		if (ar.get(n).left != 0 && ar.get(n).right != 0) {
			L = order(ar.get(n).left);
			R = order(ar.get(n).right);
		}

		if ("+-*/".contains(ar.get(n).data))
			return cal(L, R, ar.get(n).data);

		return Integer.parseInt(ar.get(n).data);
	}

	public static int cal(int l, int r, String s) {
		int result = 0;
		switch (s) {

		case "+":
			result = l + r;
			break;
		case "-":
			result = l - r;
			break;
		case "*":
			result = l * r;
			break;
		case "/":
			result = l / r;
			break;
		}
		return result;
	}

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1232.txt"));
		Scanner sc = new Scanner(System.in);

		for (int tc = 1; tc <= 10; tc++) {
			int N = sc.nextInt();
			sc.nextLine();
			ar = new ArrayList<>();
			ar.add(new Tree());

			for (int i = 0; i < N; i++) {
				String line = sc.nextLine();
				String[] str = line.split(" ");
				if (str.length == 4) {
					ar.add(new Tree(str[1]));
					ar.get(Integer.parseInt(str[0])).left = Integer.parseInt(str[2]);
					ar.get(Integer.parseInt(str[0])).right = Integer.parseInt(str[3]);
				} else {
					ar.add(new Tree(str[1]));
				}
			}
			System.out.println("#" + tc + " " + order(1));
		}
	}
}
