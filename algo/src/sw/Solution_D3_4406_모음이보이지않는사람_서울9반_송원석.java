package sw;

import java.util.Scanner;

public class Solution_D3_4406_모음이보이지않는사람_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1; tc<=T;tc++) {
			String word = sc.next();
			
			word=word.replaceAll("o", "");
			word=word.replaceAll("i", "");
			word=word.replaceAll("u", "");
			word=word.replaceAll("a", "");
			word=word.replaceAll("e", "");
			
			System.out.println("#"+tc+" "+word);
			
		}
		
	}

}


