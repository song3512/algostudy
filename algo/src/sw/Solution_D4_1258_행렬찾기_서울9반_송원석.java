package D4;

import java.io.*;
import java.util.*;

public class Solution_D4_1258_행렬찾기_서울9반_송원석 {
	static boolean[][] visit;
	static int[][] map;
	static int N;
	static ArrayList<int[]> list;
	public static void find(int x, int y) {
		int sero = x;
		int garo = y;
	label: for (int i = x; i < N; i++) {
			if (map[i][y] == 0) {
				if (sero < i - 1)
					sero = i - 1;
				break;
			}
			for (int j = y; j < N; j++) {
				if (map[i][j] == 0) {
					if (garo < j - 1)
						garo = j - 1;
					continue label;
				}
			}
		}
		list.add(new int[] {sero-x+1, garo-y+1});
		for(int i=x; i<=sero;i++) {
			for(int j=y; j<=garo;j++) {
				visit[i][j] = true;
			}
		}
		
	}

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1258.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = null;
		int T = Integer.parseInt(bf.readLine());
		for (int tc = 1; tc <= T; tc++) {
			N = Integer.parseInt(bf.readLine());
			map = new int[N][N];
			visit = new boolean[N][N];
			list = new ArrayList<>();
			for (int i = 0; i < N; i++) {
				st = new StringTokenizer(bf.readLine(), " ");
				for (int j = 0; j < N; j++) {
					map[i][j] = Integer.parseInt(st.nextToken());
				}
			}

			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if(map[i][j] != 0 && !visit[i][j]) {
						find(i,j);
					}
				}
			}
			Collections.sort(list, new Comparator<int[]>() {

				public int compare(int[] o1, int[] o2) {
					int sum1 = o1[0]*o1[1];
					int sum2 = o2[0]*o2[1];
					if(sum1 > sum2)
						return 1;
					else if(sum1 == sum2) {
						if(o1[0]>o2[0])
							return 1;
						else
							return -1;
					}
					else
						return -1;
				}
			});
			
			System.out.print("#"+tc+" "+list.size()+" ");
			for(int i=0;i<list.size();i++) {
				System.out.print(list.get(i)[0]+" "+ list.get(i)[1]+" ");
			}
			System.out.println();
		}
	}

}
