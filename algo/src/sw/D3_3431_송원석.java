package D3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class D3_3431_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_3431.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();

		for(int tc=1; tc<=T;tc++) {
			int L = sc.nextInt();
			int U = sc.nextInt();
			int X = sc.nextInt();
			int rt=0;
			
			if(X>U) 
				rt=-1;
			else if(L > X)
				rt = L-X;
			else
				rt = 0;
			
			System.out.println("#"+tc+" "+ rt);
		}
		
		
	}

}
