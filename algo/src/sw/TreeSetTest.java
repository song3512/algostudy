package com.util;

import java.util.TreeSet;
//데이터가 오름차순으로 정렬되어 저장됨(숫자-알파벳 대문자 - 소문자 - 한글)
//중복x, 입력 순서와는 상관 없음
public class TreeSetTest {

	public static void main(String[] args) {
		TreeSet<String> set2 = new TreeSet<>();
		set2.add("123");
		set2.add("홍길동");
		set2.add("BCA");
		set2.add("airplane");
		set2.add("alien");
		set2.add("apple");
		set2.add("b");
		set2.add("a");
		set2.add("camera");
		set2.add("c");
		set2.add("dance");
		set2.add("banana");
		set2.add("disc");
		
		System.out.println(set2); // 알파벳 순서대로 정렬
		System.out.println(set2.headSet("banana")); // "banana"를 기준으로 앞에 있는 값
		System.out.println(set2.headSet("banana",true)); // "banana"를 포함하는 앞에 있는 값
		System.out.println(set2.subSet("a","al" )); // "a"로 시작하고 "al"사이에 있는 부분집합
		System.out.println(set2.tailSet("c")); // "c"를 기준으로 뒤에 있는 값
		System.out.println(set2.tailSet("c",true)); // "c"를 기준으로 뒤에 있는 값

		
		
		
		
//		TreeSet<Integer> set = new TreeSet<>();
//		set.add(90);
//		set.add(35);
//		set.add(75);
//		set.add(100);
//		set.add(90);
//		set.add(88);
//		
//		System.out.println(set);
//		System.out.println(set.first());
//		System.out.println(set.last());
//		System.out.println(set.lower(90));//해당객체 바로 위에 있는 값 찾기
//		System.out.println(set.higher(80));//해당객체 바로 아래에 있는 값 찾기
//		
//		set.pollFirst(); //첫번째 객체를 꺼내고 컬렉션에서 제거
//		System.out.println(set);
//		
//		set.pollLast(); // 마지막 객체를 꺼내고 컬렉션에서 제거
//		System.out.println(set);
		
		
		
		
		
		
		
	}

}
