                                                                                                                                                                                                                                                package D3;

import java.io.FileInputStream;
import java.util.Scanner;

public class D3_8016_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_8016.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N=sc.nextInt();
			int s=1,e=1;
			int n=-1;
			for(int i=1;i<=N;i++) {
				
				for(int j=1;j<=(i*2-1);j++) {
					n+=2;
					if(j==1)
						s=n;
					if(j==i*2-1)
						e=n;
				}		
			}                                                                  
			System.out.println("#"+tc+" "+s+" "+e);
			
			
		}
		
	}

}
