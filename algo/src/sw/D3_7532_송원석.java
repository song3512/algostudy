package D3;

import java.io.FileInputStream;
import java.util.Scanner;

public class D3_7532_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_7532.txt"));
		Scanner sc = new Scanner(System.in);
		int T=sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int S=sc.nextInt();
			int E=sc.nextInt();
			int M=sc.nextInt();
			if(E==24)
				E=0;
			if(M==29)
				M=0;
			while(true) {
				if(S%24==E && S%29 ==M)
					break;
				S+=365;
			}
			System.out.println("#"+tc+" "+S);
		}
	}

}
