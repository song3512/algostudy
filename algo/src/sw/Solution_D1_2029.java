package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_2029 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2029.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	
		int T = Integer.parseInt(br.readLine().trim());
		
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int a = Integer.parseInt(st.nextToken());
			int b = Integer.parseInt(st.nextToken());
			System.out.println("#"+tc+" "+(a/b)+" "+(a%b));
		}
		
	}

}
