package a829;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Solution_D4_4408_자기방으로돌아가기_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_4408.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt(); //N명, 1,2번 방을 index 0 으로 표준화,
			int[] room = new int[200]; // 399,400번 방은 index 199에 대응
			
			for(int i=0;i<N;i++) {
				int a = (sc.nextInt()-1)/2; // 현재방번호(<=400)
				int b = (sc.nextInt()-1)/2; // 돌아가야 할 방번호(<=400)
				if(a>b) {
					int t=a;
					a=b;
					b=t;
				}
				for(int j=a;j<=b;j++)
					room[j]++;
			}
			System.out.println("#"+tc+" "+Arrays.stream(room).max().getAsInt());
		}
	}

}
