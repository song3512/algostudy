package sw;

import java.util.*;


public class Solution2 {
	static int min;
    public static int solution(int n) {
        int ans = 0;
        min = n;
        dfs(0,n,0);
        ans = min;

        return ans;
    }

    public static void dfs(int start,int end,int bat) {
	if(start == end) {
		min = Math.min(min, bat);
		return;
	}
	if(start*2 <= end && start != 0)
		dfs(start*2,end,bat);
	for(int i=end-start;i>0;i--) {
		dfs(start+i,end,bat+i);
	}
	return;
    }
    
	public static void main(String[] args) {
		System.out.println(solution(100));
	}

}
