package D3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D3_4466_송원석 {

	
	public static int[] sort(int[] num) {
		int[] result= num;
		
		int temp;
		for(int i=0;i<result.length-1;i++) {
			for(int j=i+1;j<result.length;j++) {
				if(result[i] > result[j]) {
					temp = result[i];
					result[i] = result[j];
					result[j] = temp;
				}
			}
		}
		return result;
	}
	
	
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_4466.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N=sc.nextInt();
			int K=sc.nextInt();
			int sum=0;
			int[] score= new int[N];
			
			for(int i=0;i<N;i++) {
				score[i] = sc.nextInt();
			}
			System.out.println(Arrays.toString(score));
			score = sort(score);
			
			for(int i=0;i<K;i++) {
				sum += score[N-1-i];
			}
			System.out.println("#"+tc+" "+sum);
			System.out.println(Arrays.toString(score));
			
		}
	}

}
