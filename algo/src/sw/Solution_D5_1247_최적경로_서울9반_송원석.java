package D5;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D5_1247_최적경로_서울9반_송원석 {
	static int min;
	static boolean visit[];
	static int N;
	static int[] start;
	static int[] home;
	static int[][] to;

	public static void dfs(int x, int y, int count, int distance) {
		if (count == N) {
			distance = distance + Math.abs(x - home[0]) + Math.abs(y - home[1]);
			min = Math.min(min, distance);
		}
		for (int i = 0; i < N; i++) {
			if (visit[i] == false) {
				visit[i] = true;
				dfs(to[i][0], to[i][1], count + 1, distance + Math.abs(x - to[i][0]) + Math.abs(y - to[i][1]));
				visit[i] = false;
			}
		}
	}
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1247.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int tc = 1; tc <= T; tc++) {
			N = sc.nextInt();
			start = new int[2];
			home = new int[2];
			to = new int[N][2];
			visit = new boolean[N];
			min = 9999;
			start[0] = sc.nextInt();
			start[1] = sc.nextInt();
			home[0] = sc.nextInt();
			home[1] = sc.nextInt();
			for (int i = 0; i < N; i++) {
				to[i][0] = sc.nextInt();
				to[i][1] = sc.nextInt();
			}
			dfs(start[0], start[1], 0, 0);
			System.out.println("#" + tc + " " + min);
		}
	}
}
