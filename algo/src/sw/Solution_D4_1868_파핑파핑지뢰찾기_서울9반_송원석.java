package sw;

import java.io.*;
import java.util.*;

public class Solution_D4_1868_파핑파핑지뢰찾기_서울9반_송원석 {
	static int N,min,result;
	static int[] dx = {-1,-1,0,1,1,1,0,-1}, dy= {0,1,1,1,0,-1,-1,-1};
	static char[][] map;
	
	public static void solve(){
		
		for(int i=0;i<N;i++) {
			for(int j=0;j<N;j++) {
				if(map[i][j] =='.') {
					if(checkZero(i,j)) {
						checkTrap(i,j);
						result++;
					}
				}
			}
		}
		for(int i=0;i<N;i++) {
			for(int j=0;j<N;j++) {
				if(map[i][j] =='.')
					result++;
			}
		}
		
	}
	public static boolean checkZero(int x, int y) {
		int cnt=0;
		for(int i=0;i<8;i++) {
			int nx=x+dx[i];
			int ny=y+dy[i];
			if(nx>=0 && nx <N && ny>=0 && ny <N 
					&& map[nx][ny] == '*')
				cnt++;
		}
		if(cnt==0)
			return true;
		else
			return false;
	}
	
	public static void checkTrap(int x, int y) {
		int cnt=0;
		for(int i=0;i<8;i++) {
			int nx=x+dx[i];
			int ny=y+dy[i];
			if(nx>=0 && nx <N && ny>=0 && ny <N 
					&& map[nx][ny] == '*')
				cnt++;
		}
		if(cnt==0) {
			map[x][y] ='0';
			for(int i=0;i<8;i++) {
				int nx=x+dx[i];
				int ny=y+dy[i];
				if(nx>=0 && nx <N && ny>=0 && ny <N &&map[nx][ny] == '.')
				checkTrap(nx,ny);
			}
		}else {
			map[x][y] = (char)(cnt+'0');
		}
	}




	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_1868.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		for(int tc=1;tc<=T;tc++) {
			N= Integer.parseInt(bf.readLine());
			result =0;
			map = new char[N][N];
			for(int i=0;i<N;i++) {
				map[i] = bf.readLine().toCharArray();
			}
			solve();
			System.out.println("#"+tc+" "+result);
		}//tc
	
	}
}
