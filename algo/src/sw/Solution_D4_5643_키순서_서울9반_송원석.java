package sw;

import java.io.*;
import java.util.*;

public class Solution_D4_5643_키순서_서울9반_송원석 {
	static int N;
	static int[][] graph;
	static boolean[] v;
	public static void solve(int r) {
		for (int i = 0; i < N; i++) {
			if (r != i && graph[r][i] == 1 && !v[i]) {
				v[i] = true;
				solve(i);
			}
		}
	}

	public static void solve2(int r) {
		for (int i = 0; i < N; i++) {
			if (r != i && graph[i][r] == 1 && !v[i]) {
				v[i] = true;
				solve2(i);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_5643.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine().trim());
		for (int tc = 1; tc <= T; tc++) {
			N = Integer.parseInt(bf.readLine().trim());
			int result=0;
			int M = Integer.parseInt(bf.readLine().trim());
			graph = new int[N][N];
			for (int i = 0; i < M; i++) {
				StringTokenizer st = new StringTokenizer(bf.readLine());
				int n1 = Integer.parseInt(st.nextToken()) - 1;
				int n2 = Integer.parseInt(st.nextToken()) - 1;
				graph[n1][n2] = 1;
			}
			for (int i = 0; i < N; i++) {
				v = new boolean[N];
				boolean flag = true;
				v[i] = true;
				solve(i);
				solve2(i);
				for (int j = 0; j < N; j++) {
					if (!v[j]) {
						flag = false;
						break;
					}
				}
				if (flag)
					result++;
			}
			System.out.println("#" + tc + " " + result);
		}
	}

}
