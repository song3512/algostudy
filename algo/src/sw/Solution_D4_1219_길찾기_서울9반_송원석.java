package D4;
import java.util.Scanner;
import java.io.FileInputStream;

public class Solution_D4_1219_길찾기_서울9반_송원석 {
	public static boolean[] visited;
	public static int[][] graph;
	public static boolean flag;
	public static boolean dfsr(int node) {
		if(visited[99])
			flag=true;
		visited[node]=true;
		
		for(int next=0;next<100;next++)
			if(visited[next] == false && graph[node][next] ==1)
				dfsr(next);
		return flag;
	}
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1219.txt"));
		Scanner sc= new Scanner(System.in);
		for(int tc=0; tc<10;tc++) {
			int T=sc.nextInt();
			int V=sc.nextInt();
			graph = new int[100][100];
			visited = new boolean[100];
			flag = false;
			
			for(int i=0;i<V;i++) {
				int v1=sc.nextInt();
				int v2=sc.nextInt();
				graph[v1][v2] = 1;
			}
			System.out.println("#"+T+" "+((dfsr(0))?1:0));
		}
	}
}
