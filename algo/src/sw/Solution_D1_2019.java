package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_2019 {
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	
		int num = Integer.parseInt(br.readLine());
		int result = 1;
		System.out.print(result+" ");
		for(int i=0;i<num;i++) {
			result*=2;
			System.out.print(result+" ");
		}
	}

}
