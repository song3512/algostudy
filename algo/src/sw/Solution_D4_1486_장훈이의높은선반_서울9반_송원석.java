package a1004;

import java.util.*;
import java.io.*;


public class Solution_D4_1486_장훈이의높은선반_서울9반_송원석 {
	static int[] H;
	static boolean[] V;
	static int N,B, min;
	public static void dfs(int start,int total) {
		if(total >= B) {
			min = Math.min(min, total);
			return;
		}
		
		for(int i=start;i<N;i++) {
			if(!V[i]) {
				V[i] = true;
				dfs(i,total+H[i]);
				V[i]= false;
			}
		}
		
	}
	

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1486.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(bf.readLine());
		
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			N = Integer.parseInt(st.nextToken());
			B = Integer.parseInt(st.nextToken());
			H = new int[N];
			V = new boolean[N];
			min = Integer.MAX_VALUE/2;
			st = new StringTokenizer(bf.readLine());
			
			for(int i=0;i<N;i++) {
				H[i] = Integer.parseInt(st.nextToken());
			}
			dfs(0,0);
			
			System.out.println("#"+tc+" "+(min-B));
		}
	}

}
