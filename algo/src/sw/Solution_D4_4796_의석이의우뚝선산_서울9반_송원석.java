package a0926;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D4_4796_의석이의우뚝선산_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_4796.txt"));
		Scanner sc = new Scanner(System.in);

		int T = sc.nextInt();

		for (int tc = 1; tc <= T; tc++) {
			int N = sc.nextInt();
			int[] mt = new int[N];
			int result = 0;
			for (int i = 0; i < N; i++) {
				mt[i] = sc.nextInt();
			}
			
			for(int i=1;i<N-1;i++) {
				int left=0;
				int right=0;
				if(mt[i] > mt[i-1] && mt[i+1] < mt[i]) {
					for(int l = i; l>=1; l--) {
						if(mt[l-1] > mt[l])
							break;
						left++;
					}
					for(int r = i; r<N-1; r++) {
						if(mt[r+1] > mt[r])
							break;
						right++;
					}
				}
				result += left*right;
			}
			System.out.println("#"+tc+" "+result);
		} // tc

	}

}
