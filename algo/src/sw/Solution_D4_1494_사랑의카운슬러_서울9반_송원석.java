package D4;

import java.io.*;
import java.util.*;


public class Solution_D4_1494_사랑의카운슬러_서울9반_송원석 {
	static boolean visit[];
	static int[][] worm;
	static int N;
	static long min;
	static int[][] ar;
	
	public static void comb(int start,int r) {
		if(r == N/2) {
			getVector();
			return;
		}
		
		for(int i=start;i<N;i++) {
			if(!visit[i]) {
				visit[i] = true;
				comb(i+1,r+1);
				visit[i] = false;
			}
		}
	}
	
	public static void getVector() {
		long x=0;
		long y=0;
		for(int i=0;i<N;i++) {
			if(!visit[i]) {
				x += worm[i][0];
				y += worm[i][1];
			}
			else {
				x -= worm[i][0];
				y -= worm[i][1];
			}
		}
		min = Math.min(min, x*x+y*y);
	}
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1494.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(bf.readLine());
		for(int tc=1;tc<=T;tc++) {
			N = Integer.parseInt(bf.readLine());
			worm = new int[N][2];
			StringTokenizer st;
			visit = new boolean[N];
			min = Long.MAX_VALUE;
			for(int i=0;i<N;i++) {
				st= new StringTokenizer(bf.readLine()," ");
				worm[i][0] = Integer.parseInt(st.nextToken());
				worm[i][1] = Integer.parseInt(st.nextToken());
			}
			comb(0,0);
			System.out.println("#"+tc+" "+min);
			
			
		}
	}

}
