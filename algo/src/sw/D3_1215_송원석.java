package D3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class D3_1215_송원석{

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1215.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1; tc<=10;tc++) {
			int N=sc.nextInt();
			char[][] ar = new char[8][8];
			int count=0;
			String line = new String();
			int dif = N/2; // 
			boolean flag1 = true; // 회문여부
			for(int i=0; i<8;i++) {
				line = sc.next();  //한줄 읽어옴
				for(int j=0; j<8;j++) {
					ar[i][j] = line.charAt(j);  //line에 한글자씩 배열에 넣음
				}
			}
			for(int i=0; i<8;i++) {
				for(int j=0; j<8; j++) {
					//좌표별로 우측, 아래 회문인지 확인
					flag1 = true; // 초기값
					if(j+N-1<8) { //자기포함 우측으로 N칸이 배열 범위 안에 있는지 확인
						for(int k=0; k<dif;k++) {
							if(ar[i][j+k] != ar[i][j+N-k-1]) {
								flag1=false; // 회문아니면 끝
								break;
							}
						}
						if(flag1) count++; // 회문이면 count++
						}
					
					flag1 = true;	
					if(i+N-1 <8) {//자기포함 아래측로 N칸이 배열 범위 안에 있는지 확인
						for(int k=0; k<dif;k++) {
							if(ar[i+k][j] != ar[i+N-k-1][j]) {
								flag1=false;
								break;
							}
						}
						if(flag1) count++;	
						}
					}
				}
			System.out.println("#"+tc+" "+count);
		}
	}
}
