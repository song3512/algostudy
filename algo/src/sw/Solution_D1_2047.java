package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_2047 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2047.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		String line = br.readLine().toUpperCase();			
		System.out.println(line);
	}

}
