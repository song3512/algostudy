package a1004;

import java.util.*;
import java.io.*;


public class Solution_D4_3074_입국심사_서울9반_송원석3 {
	static int N,M;
	static long result, max;
	static long[] time;
	
	
	public static void binarySearch(long stime, long etime) {
		long middle = (stime + etime)/2;
		long check = 0;
		for(int i=0;i<N;i++) {
			check += middle/time[i];
		}
		if(check < M)
			stime = middle +1; //시간이 모자람 , 오른쪽
//			return 	binarySearch(middle+1,etime);
		else { // 시간이 남아돔
			
			result = middle; // 최대시간 줄여줌
			etime = middle-1;// 왼쪽 절반으로
//			return 	binarySearch(stime,middle-1);
		}
		if(stime <= etime) binarySearch( stime,  etime);
	}
	
	

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_3074.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(bf.readLine());
		
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			N = Integer.parseInt(st.nextToken());
			M = Integer.parseInt(st.nextToken());
			time = new long[N];
			max=0;
			for(int i=0;i<N;i++) {
				time[i] = Integer.parseInt(bf.readLine());
				max = Math.max(max, time[i]);
			}
			long stime = 0;
			long etime = (long)max*M;
			binarySearch(stime,etime);
			System.out.println("#"+tc+" "+result);
		}
	}

}
