package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_2025 {
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2025.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	
		int num = Integer.parseInt(br.readLine());
		int sum =0;
		for(int i=1;i<=num;i++)
			sum+= i;
		System.out.println(sum);
	}

}
