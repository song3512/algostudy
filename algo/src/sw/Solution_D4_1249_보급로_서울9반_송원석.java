package a1010;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Solution_D4_1249_보급로_서울9반_송원석 {
	static int N;
	static int[][] map;
	static boolean[][] vmap;
	static int min_dist;
	static int[] di = { -1, 1, 0, 0 };
	static int[] dj = { 0, 0, -1, 1 };

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine().trim());
		for (int tc = 0; tc < T; tc++) {
			N = Integer.parseInt(br.readLine().trim());
			map = new int[N][N];
			vmap = new boolean[N][N];
			min_dist = Integer.MAX_VALUE;
			for (int i = 0; i < N; i++) {
				String s = br.readLine().trim();
				for (int j = 0; j < N; j++) {
					map[i][j] = (s.charAt(j) - '0');
				}
			}

			bfs();

			System.out.println("#" + (tc + 1) + " " + min_dist);
		}
	}

	static void bfs() {
		PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return Integer.compare(o1[2], o2[2]);
			}
		});
		vmap[0][0] = true;
		pq.offer(new int[] { 0, 0, 0 });
		while (!pq.isEmpty()) {
			int[] t = pq.poll();
			for (int dir = 0; dir < 4; dir++) {
				int ni = t[0] + di[dir];
				int nj = t[1] + dj[dir];
				if (ni >= 0 && ni < N && nj >= 0 && nj < N && !vmap[ni][nj]) {
					if (ni == N - 1 && nj == N - 1) {
						min_dist = t[2];
						return;
					}
					vmap[ni][nj] = true;
					pq.offer(new int[] { ni, nj, t[2] + map[ni][nj] });
				}
			}
		}
	}
}