package a0925;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Solution_D4_4366_정식이의은행업무_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_4366.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

		int T = Integer.parseInt(bf.readLine());
		for (int tc = 1; tc <= T; tc++) {
			char[] bi = bf.readLine().toCharArray();
			char[] tri = bf.readLine().toCharArray();
			char[] n = { '0', '1', '2' };
			long num = 0;
	label: for (int i = 0; i < bi.length; i++) {
				for (int k = 0; k < 2; k++) {
					if (bi[i] != n[k]) {
						char temp = bi[i];
						bi[i] = n[k];
						num = Long.valueOf(String.valueOf(bi), 2);
						bi[i] = temp;
					}
				}
				for (int j = 0; j < tri.length; j++) {
					for (int k = 0; k < 3; k++) {
						if (tri[j] != n[k]) {
							char temp = tri[j];
							tri[j] = n[k];
							if (Long.valueOf(String.valueOf(tri), 3) == num) {
								break label;
							}
							tri[j] = temp;
						}
					}
				}
			}
			System.out.println("#" + tc + " " + num);
		}
	}
}
