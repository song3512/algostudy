package a829;

import java.io.FileInputStream;
import java.util.Scanner;

class Tank {
	int x;
	int y;
	int dr;
}

public class Solution_D3_1873_상호의배틀필드_서울9반_송원석 {
	static char[][] map;
	static Tank tank;
	static int[] di = { -1, 1, 0, 0 };
	static int[] dj = { 0, 0, -1, 1 };
	static int H;
	static int W;

	public static void command(char c) {
		if ("UDLR".contains(String.valueOf(c))) {
			if (c == 'U') {
				tank.dr = 0;
				map[tank.x][tank.y] = '^';
			} else if (c == 'D') {
				tank.dr = 1;
				map[tank.x][tank.y] = 'v';
			} else if (c == 'L') {
				tank.dr = 2;
				map[tank.x][tank.y] = '<';
			} else if (c == 'R') {
				tank.dr = 3;
				map[tank.x][tank.y] = '>';
			}
			move();
		} else {
			attack();
		}
	}
	public static void move() {
		int nx = tank.x + di[tank.dr];
		int ny = tank.y + dj[tank.dr];
		if (nx < H && nx >= 0 && ny < W && ny >= 0 && map[nx][ny] == '.') {
			map[nx][ny] = map[tank.x][tank.y];
			map[tank.x][tank.y] = '.';
			tank.x = nx;
			tank.y = ny;
		}
	}
	public static void attack() {
		int nx = tank.x;
		int ny = tank.y;
		while (true) {
			if (nx + di[tank.dr] < H && nx + di[tank.dr] >= 0 && ny + dj[tank.dr] < W && ny + dj[tank.dr] >= 0) {
				nx += di[tank.dr];
				ny += dj[tank.dr];
				if (nx >= H || nx < 0 || ny >= W || ny < 0)
					return;
				if (map[nx][ny] == '#')
					return;
				if (map[nx][ny] == '*') {
					map[nx][ny] = '.';
					return;
				}
			}
			else
				return;
		}
	}

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1873.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int tc = 1; tc <= T; tc++) {
			tank = new Tank();
			H = sc.nextInt();
			W = sc.nextInt();
			map = new char[H][W];
			for (int i = 0; i < H; i++) {
				String line = sc.next();
				for (int j = 0; j < W; j++) {
					map[i][j] = line.charAt(j);
					if ("<>v^".contains(String.valueOf(map[i][j]))) {
						tank.x = i;
						tank.y = j;
						if (map[i][j] == '^')
							tank.dr = 0;
						else if (map[i][j] == 'v')
							tank.dr = 1;
						else if (map[i][j] == '<')
							tank.dr = 2;
						else
							tank.dr = 3;
					}
				}
			}
			int N = sc.nextInt();
			String com = sc.next();
			for (int i = 0; i < N; i++)
				command(com.charAt(i));
			System.out.print("#" + tc + " ");
			for (int i = 0; i < H; i++)
				System.out.println(new String(map[i]));
		}
	}

}
