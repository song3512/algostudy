package a1011;

import java.io.*;
import java.util.*;

public class Solution_D4_7701_염라대왕의이름정렬_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_7701.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		
		for (int tc = 1; tc <= T; tc++) {
			int N = Integer.parseInt(bf.readLine());
			HashSet<String> set = new HashSet<>();
			PriorityQueue<String> que = new PriorityQueue<>(new Comparator<String>() {
				public int compare(String o1, String o2) {
					if (o1.length() == o2.length()) {
						return o1.compareTo(o2);
					}
					return Integer.compare(o1.length(), o2.length());
				}
			});
			
			for (int i = 0; i < N; i++) {
				String word = bf.readLine();
				if (!set.contains(word)) { // !que.contains(word) 으로 하면 시간초과
					set.add(word);
					que.add(word);
				}
			}
			StringBuilder sb = new StringBuilder();
			sb.append("#" + tc+"\n");
			while(!que.isEmpty())
				sb.append(que.poll()+"\n");
			System.out.print(sb);
		}
	}

}
