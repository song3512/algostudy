package info;

public class factorial {
	static long[] fac;
	public static long fac(int n) {
		fac = new long[n+1];
		
		fac[0] = 1;
		
		for(int i=1;i<=n;i++) fac[i] = fac[i-1]*i;
		return fac[n];
		
	}
	public static void main(String[] args) {
		
		
		System.out.println(fac(8));
		
		
	}

}
