package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_2050 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2050.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String line = br.readLine();
			
		for(int i=0;i<line.length();i++) {
			System.out.print(line.charAt(i)-'A'+1+" ");
		}
		
	}

}
