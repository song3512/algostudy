package a1011;

import java.util.*;
import java.io.*;

public class Solution_D5_7396_종구의딸이름짓기_서울9반_송원석2 {
	static int[] di = {0,1}, dj= {1,0};
	static char[][] board;
	static int N,M;
	static String name;
	
	static void bfs() {
		PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return Integer.compare(o1[2], o2[2]);
			}
		});
		
		pq.offer(new int[] { 0, 0, board[0][0]-'A' });
		name = "";
		while (!pq.isEmpty()) {
			int[] t = pq.poll();
			name = name+board[t[0]][t[1]];
			for (int dir = 0; dir < 2; dir++) {
				int ni = t[0] + di[dir];
				int nj = t[1] + dj[dir];
				if (ni < N && nj < M) {
					if (ni == N - 1 && nj == M - 1) {
						return;
					}
					pq.offer(new int[] { ni,nj, t[2]+board[ni][nj] -'A' });
				}
			}
		}
	}
	
	
	
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_7396.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(bf.readLine());
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			N = Integer.parseInt(st.nextToken());
			M = Integer.parseInt(st.nextToken());
			board = new char[N][M];
			
			for(int i=0;i<N;i++)
				board[i] = bf.readLine().toCharArray();
			bfs();
			System.out.println("#"+tc+" "+name);
		}
		
		
	}

}
