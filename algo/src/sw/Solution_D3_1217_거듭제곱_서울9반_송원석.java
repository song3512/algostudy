package D3;
import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D3_1217_거듭제곱_서울9반_송원석 {
	public static int result(int n, int m) {
		if(m==1)
			return n;
		return n*result(n,m-1) ;
	}
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1217.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=0;tc<10;tc++) {
			int T = sc.nextInt();
			int N=sc.nextInt();
			int M=sc.nextInt();
			System.out.println("#"+T+" "+result(N,M));
		
		}
	}

}
