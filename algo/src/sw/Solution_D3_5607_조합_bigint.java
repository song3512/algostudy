package info;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.Scanner;

/*
Fermat Little Theorem
a^p = a(mod p)
a^(p-1) = 1;
a^(p-2) = 1/a;

5C2%7 -> 5!/(3!*2!)%7
	  -> 5!*6^5*2^5%7	
	  -> (5!%7)*(6^5%7)*(2^5%7)
	  -> (5*4*3*2*1%7)*(6^5%7)*(2^5%7)
	  -> (5*2*1%7)*(3*2)*(6^5%7)*2(2^5%7)
	  -> (10%7)*(6^6%7)*(2^6%7)
	  -> 3%7 * 1%7 * 1%7
	  -> 3%7

*/
public class Solution_D3_5607_조합_bigint {

	
	
	public static BigInteger nCr(int n,int r,int p) {
		long[] fac = new long[n+1];
		fac[0] = 1;
		for(int i=1;i<=n;i++) fac[i]=fac[i-1]*i%p;
		
		BigInteger P = BigInteger.valueOf(p);
		BigInteger A = BigInteger.valueOf(fac[n]);
		BigInteger B = BigInteger.valueOf(fac[r]).modInverse(P).remainder(P);
		BigInteger C = BigInteger.valueOf(fac[n-r]).modInverse(P).remainder(P);
		//페르마
	//	return (fac[n]%p*modInverse(fac[r],p)%p*modInverse(fac[n-r],p)%p)%p;
		return A.multiply(B).multiply(C).remainder(P);
		
	}
	
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_5607.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int P =1234567891;
		for(int tc=1; tc<=T;tc++) {
			int N = sc.nextInt();
			int R = sc.nextInt();
			//nCr(N,R,P);
			System.out.println("#"+tc+" "+nCr(N,R,P));
			
			
		}
		
		
	}

}
