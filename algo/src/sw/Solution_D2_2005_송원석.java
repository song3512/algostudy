package D2;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D2_2005_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2005.txt"));
		Scanner sc = new Scanner(System.in);

		int T = sc.nextInt();

		for (int tc = 1; tc <= T; tc++) {
			int N = sc.nextInt();
			int[][] tri = new int[N][];

			for (int i = 0; i < N; i++) {
				tri[i] = new int[i + 1];
				for (int j = 0; j < tri[i].length; j++) {
					if (j == 0 || j == tri[i].length - 1)
						tri[i][j] = 1;
					else if (i > 0 && j > 0)
						tri[i][j] = tri[i - 1][j - 1] + tri[i - 1][j];
				}
			}
			System.out.println("#" + tc);
			for (int[] s : tri) {
				for (int ss : s)
					System.out.print(ss + " ");
				System.out.println();
			}

		}
	}

}
