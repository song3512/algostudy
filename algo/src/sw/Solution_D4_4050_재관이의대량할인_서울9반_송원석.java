package sw;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

public class Solution_D4_4050_재관이의대량할인_서울9반_송원석 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_4050.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		for(int tc=1; tc<=T;tc++) {
			int N = Integer.parseInt(bf.readLine());
			StringTokenizer st = new StringTokenizer(bf.readLine());
			ArrayList<Integer> list = new ArrayList<>();
			int sum=0;
			for(int i=0;i<N;i++) {
				list.add(Integer.parseInt(st.nextToken()));
				sum+= list.get(i);
			}
			Collections.sort(list);
			
			for(int i=N-1;i>=0;i-=3) {
				if(i >1)
					sum -= list.get(i-2);
			}
			System.out.println("#"+tc+" "+sum);
			
		}//tc
		
		
	}

}
