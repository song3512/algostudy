package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_2056 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2056.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			String line = br.readLine();
			int month = Integer.parseInt(line.substring(4, 6));
			int day = Integer.parseInt(line.substring(6));
			boolean check = true;
			
			if(month == 1 || month == 3 ||month == 5||month == 7||month == 8||month == 10||month == 12) {
				if(day>31 || day<1)
					check = false;
			}else if(month == 2) {
				if(day>28 || day<1)
					check = false;
			}else if(month<=0){
				check = false;
			}else {
				if(day>30 || day<1)
					check = false;
			}
			
			if(check) {
				String result=line.substring(0, 4)+"/"+line.substring(4, 6)+"/"+line.substring(6);
				System.out.println("#"+tc+" "+result);
			}else {
				System.out.println("#"+tc+" "+(-1));
			}
			
			
			
			
			
		}
		
	}

}
