package D3;
import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D3_1217_서울9반_송원석2 {
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1217.txt"));
		Scanner sc = new Scanner(System.in);
		for(int tc=0;tc<10;tc++) {
			int T = sc.nextInt();
			int N=sc.nextInt();
			int M=sc.nextInt();
			int[] memo = new int[M+1];
			memo[0]=1;
			for(int i=1;i<=M;i++) {
				memo[i] = memo[i-1]*N;
			}
			System.out.println("#"+T+" "+memo[M]);
		}
	}

}
