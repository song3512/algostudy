package D2;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D2_1970_쉬운거스름돈_서울9반_송원석 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_1970.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T= sc.nextInt();
		
		for(int tc=1; tc<=T;tc++) {
			int N= sc.nextInt();
			int[] result = new int[8];
			int[] won = {50000,10000,5000,1000,500,100,50,10};
			for(int i=0;i<8;i++) {
				result[i] = N/won[i];
				N -= result[i]*won[i];
			}
			System.out.println("#"+tc);
			for(int s : result)
				System.out.print(s+" ");
			System.out.println();
		}
	}

}
