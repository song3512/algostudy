package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_1938 {
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1938.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		int b = Integer.parseInt(st.nextToken());
		
		System.out.println(a+b);
		System.out.println(a-b);
		System.out.println(a*b);
		System.out.println(a/b);
		
	}

}
