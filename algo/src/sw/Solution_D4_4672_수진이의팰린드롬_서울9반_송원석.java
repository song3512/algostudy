package a0925;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution_D4_4672_수진이의팰린드롬_서울9반_송원석 {

	public static int f(int num) {
		return num*(num+1)/2;
	}
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_4672.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());

		for (int tc = 1; tc <= T; tc++) {
			char[] ar = bf.readLine().toCharArray();
			Arrays.sort(ar);
			int result=0;
			
			int[] num = new int[26];
			int index=0;
			char c = ar[0];
			int cnt=0;
			for(int i=0;i<ar.length;i++) {
				if(ar[i] != c) {
					num[index] = cnt;
					c= ar[i];
					cnt = 0;
					index++;
				}
				cnt++;
			}
			num[index] = cnt;
			for(int i=0;i<26;i++) {
				if(num[i] ==0)
					break;
				result += f(num[i]);
			}
			
			System.out.println("#" + tc + " " + result);
		}
	}
}
