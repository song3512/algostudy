package a1011;

import java.util.*;
import java.io.*;

class Point {
	int x, y;
	String str;

	public Point(int x, int y, String str) {
		this.x = x;
		this.y = y;
		this.str = str;
	}
}

public class Solution_D5_7396_종구의딸이름짓기_서울9반_송원석 {
	static int[] di = { 0, 1 }, dj = { 1, 0 };
	static char[][] board;
	static boolean[][] visit;
	static int N, M;
	static String name;

	static void bfs(Point a) {
		PriorityQueue<Point> pq = new PriorityQueue<>(new Comparator<Point>() {
			@Override
			public int compare(Point o1, Point o2) {
				return o1.str.compareTo(o2.str);
			}
		});

		pq.offer(new Point(a.x, a.y, a.str));
		visit[0][0] = true;
		name = a.str;
		while (!pq.isEmpty()) {
			Point p = pq.poll();
			for (int dir = 0; dir < 2; dir++) {
				int ni = p.x + di[dir];
				int nj = p.y + dj[dir];
				if (ni < N && nj < M && !visit[ni][nj]) {
					if (ni == N - 1 && nj == M - 1) {
						name = p.str + board[ni][nj];
						return;
					}
					visit[ni][nj] = true;
					pq.offer(new Point(ni, nj, p.str + board[ni][nj]));
				}
			}
		}
	}

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_7396.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

		int T = Integer.parseInt(bf.readLine());
		for (int tc = 1; tc <= T; tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			N = Integer.parseInt(st.nextToken());
			M = Integer.parseInt(st.nextToken());
			board = new char[N][M];
			visit = new boolean[N][M];
			for (int i = 0; i < N; i++)
				board[i] = bf.readLine().toCharArray();

			bfs(new Point(0, 0, "" + board[0][0]));

			System.out.println("#" + tc + " " + name);
		}
	}

}
