package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_1933 {
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	
		
		int num = Integer.parseInt(br.readLine());
		
		for(int i=1;i<=num;i++) {
			if(num%i == 0)
				System.out.print(i+" ");
		}
	}

}
