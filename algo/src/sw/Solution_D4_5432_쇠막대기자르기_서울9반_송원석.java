package D4;
import java.util.Scanner;
import java.io.FileInputStream;
import java.util.Stack;

public class Solution_D4_5432_쇠막대기자르기_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_5432.txt"));
		Scanner sc= new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			Stack<Character> stack = new Stack<Character>();
			String line = sc.next();
			int cnt=0;
			stack.push(line.charAt(0));
			for(int i=1; i<line.length();i++) {
				char c1 = line.charAt(i);
				char c2 = line.charAt(i-1);
				
				if(c1 == '(')
					stack.push(c1);
				if(c1 ==')') {
					if(c2 ==')') {
					stack.pop();
					cnt++;
					}
					else {
						stack.pop();
						cnt += stack.size();
					}
				}
			}
			System.out.println("#"+tc+" "+cnt);
		}
	}
}
