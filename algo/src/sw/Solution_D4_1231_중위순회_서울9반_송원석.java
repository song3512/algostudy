package D4;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution_D4_1231_중위순회_서울9반_송원석 {
	static ArrayList<String> ar; 
	
	
	
public static void inorder(int i){
	if(i< ar.size()) {
		inorder(2*i);
		System.out.print(ar.get(i));
		inorder(2*i+1);
	}
}
		
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1231.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=10;tc++) {
			ar = new ArrayList<>();
			ar.add(" ");
			int N = sc.nextInt();
			sc.nextLine();
			for(int i=0;i<N;i++) {
				String line = sc.nextLine();
				String[] word = line.split(" ");
				ar.add(word[1]);
			}
			System.out.print("#"+tc+" ");
			inorder(1);
			System.out.println();
			
		}// tc
		
		
	}

}
