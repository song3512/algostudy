package algoWS;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Solution_D4_1238_Contact_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1238.txt"));
		Scanner sc = new Scanner(System.in);

		for (int tc = 1; tc <= 10; tc++) {
			int N = sc.nextInt();
			int start = sc.nextInt();
			int[][] ar = new int[101][101];
			int max = 0;
			boolean[] visit = new boolean[101];
			for (int i = 0; i < N / 2; i++) {
				int n = sc.nextInt();
				int m = sc.nextInt();
				ar[n][m] = 1;
			}
			Queue<Integer> queue = new LinkedList<>();

			queue.offer(start);
			
			while (!queue.isEmpty()) {
				
				int size = queue.size();
				max=0;
				for(int k=0;k<size;k++) {
					
					int curr = queue.poll();
					if (curr > max)
						max = curr;
					for (int i = 1; i <= 100; i++) {
						if (ar[curr][i] == 1 && visit[i] == false) {
							queue.offer(i);
							visit[i] = true;
							
						}
					}
				}
				
			}
			System.out.println("#" + tc + " " + max);

		}

	}

}
