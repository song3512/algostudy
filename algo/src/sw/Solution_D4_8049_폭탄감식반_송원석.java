package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Solution_D4_8049_폭탄감식반_송원석 {
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_8049.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		for(int tc=1; tc<=T; tc++) {
			boolean flag=true;
			String line = bf.readLine();
			char[] word = line.toCharArray();
			char first = word[0];
			char second = word[1];
			
			if(first != 'F') 
				flag=false;
			
			for(int i=2;i<word.length;i++) {
				
			
				switch(second) {
				case 'F' :
					if(word[i] != 'F' && word[i] !='M' && (word[i] !='C' && (first == 'F' || first == 'M') ))
						flag = false;
				break;
				case 'C' :
					if(word[i] != 'C' && word[i] !='M')
						flag = false;
				break;
				case 'M' :
					if( first != 'F' && word[i] != 'F') {
						flag= false;
						break;
					}
					else if(word[i] =='F') {
						first = second;
						second = word[i];
					}
					else if(word[i] == 'C') {
						if(first !='F') {
							flag = false;
							break;}
						else {
							first = second;
							second = word[i];
						}
					}
					break;	
				}
				if(flag ==false)
					break;
				
				if(word[i] =='M' && word[i-1] == 'F') {
					first = word[i-1];
					second = word[i];
				}
				
				if(word[i] =='M' && word[i-1] == 'C') {
					first = word[i-1];
					second = word[i];
				}
				
				
			}
			
			
			System.out.println("#"+tc+" "+((flag)? "DETECTED!" : "NOTHING!"));
		}
		
		
		
		
	}
}
