package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_1936 {
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1936.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		int b = Integer.parseInt(st.nextToken());
		
		if(a==1) {
			if(b==3)
				System.out.println("A");
			else
				System.out.println("B");
		}else if(a==2) {
			if(b==1)
				System.out.println("A");
			else
				System.out.println("B");
		}else {
			if(b==2)
				System.out.println("A");
			else
				System.out.println("B");
		}
		
		
	}

}
