package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_2058 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2058.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String num = br.readLine();
		int sum=0;
		for(int i=0;i<num.length();i++) {
			sum += num.charAt(i)-'0';
		}
		
		System.out.println(sum);
	}

}
