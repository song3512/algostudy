package info;

import java.util.Arrays;
import java.util.Scanner;

//중복순열 nPIr=n^r  5PI3 = 5*5*5 = 125
//순열
//중복조합, 조합
public class PowerSet {
	public static int n,cnt,a[],v[],d[]= {1,2,3,4,5};
	
	public static void powerset(int count) { //재귀함수 첫번째 0번
		if(count==n) {
			cnt++;
			System.out.print(Arrays.toString(v)+" ");
			for(int i=0;i<n;i++){
				if(v[i]==1) System.out.print(d[i]+" ");
			}
			System.out.println();
			return;
		}
		
		for(int i=0;i<2;i++) {
			v[count]=i;
			powerset(count+1);
		}
		
//		v[count]=0;
//		powerset(count+1);
//		v[count]=1;
//		powerset(count+1);
		
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		n= 5; //sc.nextInt();
		v= new int[n]; //중복 방지
		powerset(0); //순열은 파라메터 1개면 됨
		System.out.println(cnt);
		sc.close();
		
	}

}
