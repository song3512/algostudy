package sw;

import java.io.*;
import java.util.*;

public class Solution_D4_4261_빠른휴대전화키패드_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_4261.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		String[] key = {"abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"}; // 0~7  (2~9)
		
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			char[] S = st.nextToken().toCharArray();
			int N = Integer.parseInt(st.nextToken());
			String[] dictionary = new String[N];
			st = new StringTokenizer(bf.readLine());
			for(int i=0;i<N;i++) {
				dictionary[i] = st.nextToken();
			}
			int count = 0;
			for(int i=0;i<N;i++) {
				if(dictionary[i].length() == S.length) {
					boolean check = true;
					for(int j=0;j<S.length;j++) {
						if(!(key[(S[j]-'0')-2].contains(dictionary[i].subSequence(j, j+1)))) {
							check = false;
							break;
						}
					}
					if(check)
						count++;
				}
			}
			System.out.println("#"+tc+" "+count);
		}//tc
		
		
		
	}

}
