package sw;

import java.io.*;
import java.util.*;


public class Solution_D9_5658_보물상자비밀번호_서울9반_송원석 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_5658.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			int N = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());
			int length = N/4;
			String line = bf.readLine();
			ArrayList<Integer> list = new ArrayList<>();
			for(int k=0;k<length;k++) {//한칸씩 이동. 단 K만큼만 이동
				String nums = line; 
				line = nums.charAt(N-1) + nums.substring(0,N-1); 
				int start=0;
				for(int i=0;i<4;i++) {
					start = i*length; // 시작위치 바꿔줌
					if(!list.contains(Integer.parseInt(line.substring(start,start+length),16)))//16진법
						list.add(Integer.parseInt(line.substring(start,start+length), 16));
				}
			}
			
			Collections.sort(list);	//높은 순으로 정렬
			System.out.println("#"+tc+" "+list.get(list.size()-K)); //뒤에서 K번째
		}//tc
	}
}
