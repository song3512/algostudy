package D4;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D4_3289_서로소집합_서울9반_송원석{
	
	public static int getParent(int [] p, int x) {
		if(p[x]==x)
		return x;
		else
		return p[x]=getParent(p,p[x]);
		}

		public static void unionParent(int [] p, int a, int b) {
		a = getParent(p, a);
		b = getParent(p, b);
//		if(a < b)
		p[b] = a;
//		else
//		p[a] = b;
		}

		public static boolean findParent(int [] p, int a, int b) {
		a = getParent(p, a);
		b = getParent(p, b);
		if(a == b)
		return true;
		else
		return false;
		}
		
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_3289.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int M = sc.nextInt();
			
			int[] p = new int[N+1];
			
			for(int i=1;i<=N;i++)
				p[i] = i;
			
			System.out.print("#"+tc+" ");
			for(int i=0;i<M;i++) {
				int s1 = sc.nextInt();
				int s2 = sc.nextInt();
				int s3 = sc.nextInt();
				
				if(s1 == 1) {
					if(findParent(p, s2, s3))
						System.out.print(1);
					else
						System.out.print(0);
				}
				
				else {
					unionParent(p,s2,s3);
				}
			}
			System.out.println();
			
		}//tc
		
	}

}
