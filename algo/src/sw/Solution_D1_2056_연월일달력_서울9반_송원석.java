package sw;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Solution_D1_2056_연월일달력_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2056.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		
		for(int tc=1;tc<=T;tc++) {
			String line = bf.readLine();
			int year = Integer.parseInt(line.substring(0,4));
			int month = Integer.parseInt(line.substring(4,6));
			int day = Integer.parseInt(line.substring(6));
			boolean flag =true;
			
			if(month <1 || day <1 || year <1)
				flag = false;
			else if(((month < 9 && month%2==1) || (month > 6 && month%2 ==0)) && day> 31)
				flag = false;
			else if(((month >= 9 && month%2==1) || (month <= 6 && month%2 ==0 && month > 2)) && day> 30)
				flag= false;
			else if(month==2 && day>28)
				flag =false;
			
			
			if(flag)
				System.out.printf("#%d %04d/%02d/%02d\n",tc,year,month,day);
			else
				System.out.println("#"+tc+" "+-1);
		}
		
	}

}
