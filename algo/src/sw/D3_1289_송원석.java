package D3;

import java.io.FileInputStream;
import java.util.Scanner;

public class D3_1289_송원석 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_1289.txt"));
		Scanner sc = new Scanner(System.in);
		int T= sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			String line = sc.next();
			char[] ar = new char[line.length()];
			ar = line.toCharArray();
			char su = '0'; //초기값 0;
			int cnt =0;
			
			//ar의 0번째 인덱스부터 su와 다를때마다 카운트를 해줌
			for(int i=0;i<ar.length;i++)
				if(ar[i] != su) {
					cnt++;
					su=ar[i];
					}
			System.out.println("#"+tc+" "+cnt);
		}
	}
}
