package D4;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Scanner;

//왼쪽,오른쪽 트리인덱스값 저장
class Btree {
	int left;
	int right;
	String data;

	public Btree() {
	}

	public Btree(String data) {
		this.data = data;
		this.left = 0;
		this.right = 0;
	}

	public Btree(int left, int right, String data) {
		this.left = left;
		this.right = right;
		this.data = data;
	}
}

public class Solution_D4_1233_사칙연산유효성검사_서울9반_송원석 {
	public static ArrayList<Btree> ar;

	public static boolean order(int n) {
		//참인지 거짓인지 확인하는 boolean
		boolean flagL = true;
		boolean flagR = true;
		//자식 노드가 있다면 
		if (ar.get(n).left != 0 && ar.get(n).right != 0) {
			//자식 노드가 있는데 기호가 아니면 false
			if(!("+-*/".contains(ar.get(n).data)))
				return false;
			
			//숫자라면 자식노드로 넘어감
			flagL = order(ar.get(n).left);
			flagR = order(ar.get(n).right);
			
			//자식노드 둘다 참이면 true 하나라도 거짓이면 false
			return (flagL && flagR);
		}
		
		//마지막 노드에 도착했는데 숫자가 아니면 false
		if (ar.get(n).left == 0 && ar.get(n).right == 0 && "+-*/".contains(ar.get(n).data)) {
			return false;
		}
		
		// 숫자라면 true
		return true;
	}


	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1233.txt"));
		Scanner sc = new Scanner(System.in);

		for (int tc = 1; tc <= 10; tc++) {
			int N = sc.nextInt();
			sc.nextLine();
			ar = new ArrayList<>();
			ar.add(new Btree());

			
			for (int i = 0; i < N; i++) {
				String line = sc.nextLine();
				String[] str = line.split(" ");
				if (str.length == 4) {// index, data, left 인덱스, right 인덱스
					
					//문자열 4개짜리면 1인덱스 값을 node의 data값으로
					ar.add(new Btree(str[1]));
					// 
					ar.get(Integer.parseInt(str[0])).left = Integer.parseInt(str[2]);
					ar.get(Integer.parseInt(str[0])).right = Integer.parseInt(str[3]);
					
				} else {// 2개라면 index, data
					ar.add(new Btree(str[1]));
				}
			}
			System.out.println("#" + tc + " " + ((order(1))?1:0));
		}
	}
}
