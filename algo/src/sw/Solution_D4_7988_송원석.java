package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution_D4_7988_송원석 {
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_7988.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T= Integer.parseInt(bf.readLine());
		
		for(int tc=1; tc<=T;tc++) {
			int N = Integer.parseInt(bf.readLine());
			String[][] sng = new String[N][2];
			boolean flag = false;
			ArrayList<String> memAL= new ArrayList<>();
			for(int i=0; i<N;i++) {
				String line = bf.readLine();
				String[] str = line.split(" ");
				sng[i][0] = str[0];
				sng[i][1] = str[1];
				if(!memAL.contains(sng[i][0]))
					memAL.add(sng[i][0]);
				if(!memAL.contains(sng[i][1]))
					memAL.add(sng[i][1]);
			}
			
			for(int i=1;i<(1<<memAL.size())-1;i++) {
				ArrayList<String> team1 = new ArrayList<String>();
				ArrayList<String> team2 = new ArrayList<String>();
				
				for(int j=0;j<memAL.size();j++) { 
					if((i&(1<<j)) >0) 
						team1.add(memAL.get(j));
					if((i&(1<<j)) ==0) 
						team2.add(memAL.get(j));
				}
				
				int count=0;
				for(int k=0;k<N;k++) {
					
					if(team1.contains(sng[k][0]) && team1.contains(sng[k][1])) {
						count++;
						break;
					}
					else if(team2.contains(sng[k][0]) && team2.contains(sng[k][1])) {
						count++;
						break;
					}
				}
				if(count==0)
					flag=true;
			}
			System.out.println("#"+tc+" "+ ((flag)? "Yes":"No"));
	
	}
}
}

