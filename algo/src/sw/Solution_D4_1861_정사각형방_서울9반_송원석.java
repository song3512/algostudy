package D4;

import java.io.*;
import java.util.*;

public class Solution_D4_1861_정사각형방_서울9반_송원석 {
	static int N;
	static boolean visit[][];
	static int[][] rooms;
	static int cnt ;
	public static void dfs(int x , int y) {
		int[] di = {-1,1,0,0};
		int[] dj = {0,0,-1,1};
		visit[x][y] = true;
		cnt++;
		for(int i=0;i<4;i++) {
			int nx = x+di[i];
			int ny = y+dj[i];
			if(nx >=0 && nx <N && ny >=0 && ny <N && rooms[nx][ny] == rooms[x][y]+1)
				dfs(nx,ny);
		}
	}
	
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_1861.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st;
		
		int T = Integer.parseInt(bf.readLine());
		for(int tc=1;tc<=T;tc++) {
			N = Integer.parseInt(bf.readLine());
			rooms = new int[N][N];
			visit = new boolean[N][N];
			cnt=0;
			int max=0;
			int maxNum=0;
			for(int i=0;i<N;i++) {
				st = new StringTokenizer(bf.readLine()," ");
				for(int j=0;j<N;j++) {
					rooms[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					if(!visit[i][j]) {
						dfs(i,j);
						if(max < cnt) {
							max = cnt;
							maxNum = rooms[i][j];
						}else if(max == cnt) {
							if(maxNum > rooms[i][j])
								maxNum = rooms[i][j];
						}
						cnt=0;
					}
				}
			}
			System.out.println("#"+tc+" "+maxNum+" "+max);
			
		}
	}

}
