package sw;

import java.io.*;
import java.util.*;

public class Solution_D4_4613_러시아국기같은깃발_서울9반_송원석 {
	static char[] color = { 'W', 'B', 'R' };
	static char[][] flag;
	static int N, M, min;

	public static void dfs(int r, int index, int total) {
		if (r == N) {
			min = Math.min(min, total);
			return;
		}
		for (int j = 0; j < M; j++) {
			if (flag[r][j] != color[index])
				total++;
		}
		if (index != 2) {
			if (index == 0 && r == N - 3) {
				dfs(r + 1, index + 1, total);
			} else if (index == 1 && r == N - 2) {
				dfs(r + 1, index + 1, total);
			} else {
				dfs(r + 1, index + 1, total);
				dfs(r + 1, index, total);
			}
		} else {
			dfs(r + 1, index, total);
		}

	}

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_4613.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

		int T = Integer.parseInt(bf.readLine());

		for (int tc = 1; tc <= T; tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			N = Integer.parseInt(st.nextToken());
			M = Integer.parseInt(st.nextToken());
			min = Integer.MAX_VALUE;
			int total = 0;
			flag = new char[N][M];

			for (int i = 0; i < N; i++)
				flag[i] = bf.readLine().toCharArray();

			for (int i = 0; i < M; i++)
				if (flag[0][i] != 'W')
					total++;
			dfs(1, 0, total);
			dfs(1, 1, total);
			System.out.println("#" + tc + " " + min);
		} // tc

	}

}
