package algoWS;
import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Solution_D4_1226_미로1_Bfs_서울9반_송원석 {
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1226.txt"));
		Scanner sc = new Scanner(System.in);
		for (int tc = 1; tc <= 10; tc++) {
			int T = sc.nextInt();
			int[][] ar = new int[16][16];
			int[] start = new int[2];
			boolean[][] visit = new boolean[16][16];
			boolean flag = false;
			int[] di = { -1, 1, 0, 0 };
			int[] dj = { 0, 0, -1, 1 };
			for (int i = 0; i < 16; i++) {
				String line = sc.next();
				for (int j = 0; j < 16; j++) {
					ar[i][j] = line.charAt(j) - '0';
					if (ar[i][j] == 2) {
						start[0] = i;
						start[1] = j;
					}
				}
			}
			Queue<int[]> queue = new LinkedList<>();
			queue.offer(start);
			while (!queue.isEmpty()) {
				int[] curr = queue.poll();
				visit[curr[0]][curr[1]] = true;
				if (ar[curr[0]][curr[1]] == 3) {
					flag = true;
					break;
				}
				for (int i = 0; i < 4; i++) {
					int ni = curr[0] + di[i];
					int nj = curr[1] + dj[i];
					if (ni < 16 && ni >= 0 && nj < 16 && nj >= 0 && visit[ni][nj] == false && ar[ni][nj] != 1)
						queue.offer(new int[] { ni, nj });
				}
			}
			System.out.println("#" + T + " " + ((flag) ? 1 : 0));
		} // tc

	}

}
