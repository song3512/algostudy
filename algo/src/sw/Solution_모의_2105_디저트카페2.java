package sw;

import java.io.*;
import java.util.*;

public class Solution_모의_2105_디저트카페2 {
	static int N,max;
	static int[][] cafe;
	static int[] di = {1,1,-1,-1}, dj= {-1,1,1,-1}; // 대각선 좌측아래부터 이동
	static int[] num;
	
	
	public static void move(int i, int j, int si, int sj, int dir, int cnt) {
		
		if(i==si && j == sj) {
			int ni = i+di[dir], nj = j + dj[dir];
			if(ni>=0 && ni <N && nj >=0 && nj<N && num[cafe[ni][nj]] ==0) {
				num[cafe[ni][nj]] =1;
				move(ni,nj,si,sj,dir,cnt+1);
				num[cafe[ni][nj]] =0;
			}
			return;
		}
		
		int ni = i+di[dir], nj = j + dj[dir];
		
		if(ni>=0 && ni <N && nj >=0 && nj<N && num[cafe[ni][nj]] ==0) {
			num[cafe[ni][nj]] =1;
			move(ni,nj,si,sj,dir,cnt+1);
			num[cafe[ni][nj]] =0;
		}else if(ni==si && nj == sj) {
			max = Math.max(max, cnt);
			return;
		}
		dir++;
		if(dir>3) return;
		
		ni = i + di[dir];
		nj = j + dj[dir];
		if(ni>=0 && ni <N && nj >=0 && nj<N && num[cafe[ni][nj]] ==0) {
			num[cafe[ni][nj]] =1;
			move(ni,nj,si,sj,dir,cnt+1);
			num[cafe[ni][nj]] =0;
		}else if(ni==si && nj == sj) {
			max = Math.max(max, cnt);
			return;
		}
		
		
		
	}
	
	
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_2105.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		
		for(int tc=1;tc<=T;tc++) {
			N = Integer.parseInt(bf.readLine()); 
			cafe = new int[N][N];
			max= Integer.MIN_VALUE;
			num = new int[101];
			
			StringTokenizer st = null;
			
			for(int i=0;i<N;i++) {
				st = new StringTokenizer(bf.readLine());
				for(int j=0;j<N;j++) {
					cafe[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			for(int i=0;i<N-1;i++) {
				for(int j=1;j<N-1;j++) {
					num[cafe[i][j]] =1;
					move(i,j,i,j,0,1);
					num[cafe[i][j]] =0;
				}
			}
			
			if(max == Integer.MIN_VALUE) {
				System.out.println("#"+tc+" "+-1);
			}else
				System.out.println("#"+tc+" "+max);
		}
		
	}

}
