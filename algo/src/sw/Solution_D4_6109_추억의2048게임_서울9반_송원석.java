package sw;

import java.io.*;
import java.util.*;

public class Solution_D4_6109_추억의2048게임_서울9반_송원석 {
	
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_6109.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());

		for (int tc = 1; tc <= T; tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			int N = Integer.parseInt(st.nextToken());
			String command = st.nextToken();
			int[][] board = new int[N][N];
			boolean[][] v = new boolean[N][N];
			for (int i = 0; i < N; i++) {
				st = new StringTokenizer(bf.readLine());
				for (int j = 0; j < N; j++) {
					board[i][j] = Integer.parseInt(st.nextToken());
				}
			}

			switch (command) {
			case "up":
				for (int i = 1; i < N; i++) {
					for (int j = 0; j < N; j++) {
						if (board[i][j] != 0) {
							int ni = i-1;
							int bi = i;
							while (ni >= 0) {
								if (board[ni][j] != 0) {
									if (board[ni][j] == board[bi][j] && !v[ni][j]) {
										v[ni][j] = true;
										board[ni][j] *= 2;
										board[bi][j] = 0;
										break;
									}else
										break;
								}else {
									board[ni][j] = board[bi][j];
									board[bi][j] =0;
									bi = ni;
									ni--;
								}
							}
						}
					}
				}
				break;
			case "down":
				for (int i = N-1; i >= 0; i--) {
					for (int j = 0; j < N; j++) {
						if (board[i][j] != 0) {
							int ni = i+1;
							int bi = i;
							while (ni < N) {
								if (board[ni][j] != 0) {
									if (board[ni][j] == board[bi][j] && !v[ni][j]) {
										v[ni][j] = true;
										board[ni][j] *= 2;
										board[bi][j] = 0;
										break;
									}else
										break;
								}else {
									board[ni][j] = board[bi][j];
									board[bi][j] =0;
									bi = ni;
									ni++;
								}
							}
						}
					}
				}
				break;
			case "left":
				for (int j = 0; j <N; j++) {
					for (int i = 0; i < N; i++) {
						if (board[i][j] != 0) {
							int nj = j-1;
							int bj = j;
							while (nj >=0) {
								if (board[i][nj] != 0) {
									if (board[i][nj] == board[i][bj] && !v[i][nj]) {
										v[i][nj] = true;
										board[i][nj] *= 2;
										board[i][bj] = 0;
										break;
									}else
										break;
								}else {
									board[i][nj] = board[i][bj];
									board[i][bj] =0;
									bj=nj;
									nj--;
								}
							}
						}
					}
				}
				break;
			case "right":
				for (int j = N-1; j >=0; j--) {
					for (int i = 0; i < N; i++) {
						if (board[i][j] != 0) {
							int nj = j+1;
							int bj = j;
							while (nj<N) {
								if (board[i][nj] != 0) {
									if (board[i][nj] == board[i][bj] && !v[i][nj]) {
										v[i][nj] = true;
										board[i][nj] *= 2;
										board[i][bj] = 0;
										break;
									}else
										break;
								}else {
									board[i][nj] = board[i][bj];
									board[i][bj] =0;
									bj=nj;
									nj++;
								}
							}
						}
					}
				}
				break;
			}
			System.out.println("#"+tc);
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					System.out.print(board[i][j]+" ");
				}
				System.out.println();
			}
			
		} // tc

	}

}
