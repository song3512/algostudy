package D4;

import java.io.*;
import java.util.*;

public class Solution_SW_5656_송원석 {
	static int[][] ar;
	static int W,H;

	public static void bomb(int x, int y) {
		if(x>=H || x<0 || y >=W || y<0  )
			return;
		
		int num = ar[x][y] - 1;
		int[] di = { -1, 1, 0, 0 };
		int[] dj = { 0, 0, -1, 1 };

		ar[x][y] = 0;
		if (num > 0) {
			for (int i = 0; i < 4; i++) {
				for (int j = 1; j <= num; j++) {
					int nx = x + di[i] * j;
					int ny = y + dj[i] * j;
					bomb(nx, ny);
				}
			}
		down(x,y);
		}
	}

	public static void down(int x, int y) {
		for (int i = x; i >= 1; i--) {
			ar[i][y] = ar[i - 1][y];
		}
	}

	
	
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_5656.txt"));
		Scanner sc = new Scanner(System.in);

		int T = sc.nextInt();

		for (int tc = 1; tc <= T; tc++) {
			int N = sc.nextInt();
			W = sc.nextInt();
			H = sc.nextInt();
			ar = new int[H][W];
			int sum=0;

			for (int i = 0; i < H; i++) {
				for (int j = 0; j < W; j++) {
					ar[i][j] = sc.nextInt();
				}
			}
			
			
			//bfs
			Queue<int[][]> que = new LinkedList<>();
			int[][] nar;
			
			while(N!=0) {
				
				
				
				
				N--;
			}
			
			for (int i = 0; i < H; i++) {
				for (int j = 0; j < W; j++) {
					sum += ar[i][j];
				}
			}
			System.out.println("#"+tc+" "+sum);
		} // tc

	}
}
