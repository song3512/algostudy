package a1004;

import java.util.*;
import java.io.*;


public class Solution_D4_3074_입국심사_서울9반_송원석4 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_3074.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(bf.readLine());
		
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(bf.readLine());
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			int[] time = new int[N];
			for(int i=0;i<N;i++) {
				time[i] = Integer.parseInt(bf.readLine());
			}
			int[] imgra = new int[N];
			
			for(int i=0;i<M;i++) {
				int min = Integer.MAX_VALUE/2;
				int index=0;
				for(int j=0;j<N;j++) {
					if(imgra[j]+time[j] < min) {
						min = imgra[j]+time[j];
						index = j;
					}
				}
				imgra[index] += time[index];
			}
			Arrays.sort(imgra);
			System.out.println("#"+tc+" "+imgra[N-1]);
			
		}
	}

}
