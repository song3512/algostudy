package a0925;

import java.io.*;
import java.util.*;

public class Solution_D4_3378_스타일리쉬들여쓰기_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_3378.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int p = sc.nextInt();
			int q = sc.nextInt();
			int[] R = new int[3];
			int[] C = new int[3];
			int[] S = new int[3];
			
			for(int i=0;i<p;i++) {
				String line = sc.next();
				int cnt = 0;
				for(int j=0;j<line.length();j++) {
					switch(line.charAt(j)) {
					case '(':
						R[1]++;
						break;
					case ')':
						R[2]++;
						break;
					case '{':
						C[1]++;
						break;
					case '}':
						C[2]++;
						break;
					case '[':
						S[1]++;
						break;
					case ']':
						S[2]++;
						break;
					}
				}
			}
		}
	}
}
