package D4;

import java.io.FileInputStream;
import java.util.Scanner;
import java.util.Stack;

public class Solution_D4_1218_괄호짝짓기_서울9반_송원석2 {
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1218.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1; tc<=10;tc++) {
			int N= sc.nextInt();
			String line = sc.next();
			Stack<Character> stack = new Stack<>();
			int result=1;
			for(int i=0;i<N;i++) {
				char c= line.charAt(i);
				if(c=='(' || c =='<' || c=='[' || c=='{')
					stack.push(c);
				else {
					char top = stack.pop();
					if((c==')' && top !='(') ||
					   (c=='>' && top !='<') ||	
					   (c=='}' && top !='{') ||
					   (c==']' && top !='[') ) {
						result=0;
						break;
					}
				}
			}
			System.out.println("#"+tc+" "+result);
		}
		}
	}

