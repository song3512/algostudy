package D4;

import java.io.*;
import java.util.*;

public class Solution_D4_3459_승자예측하기_서울9반_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_3459.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(bf.readLine());
		
		
		for(int tc=1;tc<=T;tc++) {
			boolean bob = true;
			long N = Long.parseLong(bf.readLine())-1;
			int base = 2;
			while(N>0) {
				N-=Math.pow(4, (base++/2));
				bob = !bob;
			}
			System.out.println("#"+tc+" "+ (bob? "Bob": "Alice"));
		}
	}

}
