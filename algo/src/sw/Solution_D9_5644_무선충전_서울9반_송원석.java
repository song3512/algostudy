package sw;

import java.io.*;
import java.util.*;

class Person{
	int x, y,cnt;
	int[] dx= {0,-1,0,1,0}, dy= {0,0,1,0,-1};
	int[] dir;
	public Person(int x, int y, int[] dir) {
		this.x =x;
		this.y =y;
		this.dir= dir;
		this.cnt = 0;
	}
	public void move(int n) {
		this.x = this.x+dx[dir[n]];
		this.y = this.y+dy[dir[n]];
	}
	public HashSet<Integer> findBC(BC[] bc) {
		HashSet<Integer> set = new HashSet<>();
		for(int i=0;i<bc.length;i++) {
			if((Math.abs(bc[i].x-this.x)+Math.abs(bc[i].y-this.y)) <= bc[i].C) {
				set.add(i);
			}
		}
		return set;
	}
}
class BC{
	int x,y,C,P;
	public BC(int x, int y, int c, int p) {
		this.x = x;
		this.y = y;
		C = c;
		P = p;
	}
}
public class Solution_D9_5644_무선충전_서울9반_송원석 {
	static Person[] p;
	static BC[] bc;
	static int M,C,result;
	
	//최대 충전값 구하기
	public static void maxCharge() {
		HashSet<Integer> set1 = p[0].findBC(bc);
		HashSet<Integer> set2 = p[1].findBC(bc);
		if(set1.size() ==0 && set2.size() ==0)
			return;
		int[] chargeA = new int[C];
		int[] chargeB = new int[C];
		int charge=0;
		for(int i=0;i<C;i++) {
			if(set1.contains(i))
				chargeA[i]=bc[i].P;
			if(set2.contains(i))
				chargeB[i]=bc[i].P;
		}
		
		for(int i=0;i<C;i++) {
			for(int j=0;j<C;j++) {
				if(i != j) {
					charge=Math.max(charge, chargeA[i]+chargeB[j]);
				}else {
					if(chargeA[i] ==0 || chargeB[j] ==0)
						charge=Math.max(charge, chargeA[i]+chargeB[j]);
					else
						charge=Math.max(charge, (chargeA[i]+chargeB[j])/2);
				}
			}
		}
		//전체 충전량에 더해줌
		result += charge;
	}
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_5644.txt"));
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(bf.readLine());
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st= new StringTokenizer(bf.readLine());
			M = Integer.parseInt(st.nextToken());
			C = Integer.parseInt(st.nextToken());
			p=new Person[2];
			bc = new BC[C];
			result=0;
			for(int k=0;k<2;k++) {
				if(M!=0)
					st= new StringTokenizer(bf.readLine());
				int[] dir= new int[M];
				for(int i=0;i<M;i++) {
					dir[i] = Integer.parseInt(st.nextToken());
				}
				if(k==0)
					p[k] = new Person(0,0,dir);
				else
					p[k] = new Person(9,9,dir);
			}
			for(int i=0;i<C;i++) {
				st= new StringTokenizer(bf.readLine());
				int y = Integer.parseInt(st.nextToken())-1;
				int x = Integer.parseInt(st.nextToken())-1;
				int c = Integer.parseInt(st.nextToken());
				int power = Integer.parseInt(st.nextToken());
				bc[i] = new BC(x,y,c,power);
			}
			maxCharge();
			for(int i=0;i<M;i++) {
				p[0].move(i);
				p[1].move(i);
				maxCharge();
			}
			System.out.println("#"+tc+" "+ result);
		}//tc
		
		
	}

}
