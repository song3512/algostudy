package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1954_송원석 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1954.txt"));
		Scanner sc = new Scanner(System.in);
		
		int[] di = {0,1,0,-1}; // 우,하,좌,상
		int[] dj = {1,0,-1,0}; // 우,하,좌,상
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int[][] visit = new int[N][N];
			int num =1;
			int ni=0;
			int nj=0;
			visit[0][0] = num++;
			boolean flag = true; //while 조건 변수
			while(flag) {
				for(int i=0; i<4;i++) { // 우,하,좌,상 순으로 이동함
					for(int j=0;j<N;j++) { // N-1까지 돌려줌
						if(ni+di[i] <N && ni+di[i] >=0 && nj+dj[i]<N && nj+dj[i]>=0 && visit[ni+di[i]][nj+dj[i]] ==0) {
							ni=ni+di[i];
							nj=nj+dj[i];
							visit[ni][nj] = num++; //배열에 num입력하고 +1해줌
						}
					}
				}
				if(num==N*N+1) // 마지막 종착지에 도착하면 N*N을 찍고 num이 +1되기 때문에 N*N+1로 조건문
					flag=false;
			}
			System.out.println("#"+tc);
			for(int[] su : visit) {
				for(int ssu : su)
					System.out.print(ssu+" ");
				System.out.println();
			}
			}
		}
	}


