package sw;

import java.io.*;
import java.util.*;

public class Solution_D1_2043 {

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_2043.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int result = Integer.parseInt(st.nextToken()) - Integer.parseInt(st.nextToken()) +1;
		
		System.out.println(result);
		
		
	}

}
